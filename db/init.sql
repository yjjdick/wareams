/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50639
 Source Host           : 127.0.0.1:3306
 Source Schema         : sdb

 Target Server Type    : MySQL
 Target Server Version : 50639
 File Encoding         : 65001

 Date: 06/11/2018 17:15:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `bas_sku`;
CREATE TABLE `bas_sku` (
  `customerid` varchar(30) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `hazard_flag` varchar(5) DEFAULT 'N',
  `descr_e` varchar(200) DEFAULT NULL,
  `descr_c` varchar(200) DEFAULT NULL,
  `packid` varchar(40) NOT NULL,
  `tare` decimal(18,8) DEFAULT NULL,
  `active_flag` varchar(1) NOT NULL DEFAULT 'N',
  `lotid` varchar(10) NOT NULL,
  `cartongroup` varchar(10) DEFAULT NULL,
  `putawayzone` varchar(10) DEFAULT NULL,
  `cube` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `putawaylocation` varchar(20) DEFAULT NULL,
  `grossweight` decimal(18,8) DEFAULT NULL,
  `netweight` decimal(18,8) DEFAULT NULL,
  `cycleclass` varchar(1) DEFAULT NULL,
  `lastcyclecount` date DEFAULT NULL,
  `reorderqty` decimal(18,3) DEFAULT '0.000',
  `price` decimal(18,8) DEFAULT NULL,
  `shelflifeflag` varchar(1) DEFAULT 'N',
  `inboundlifedays` int(11) DEFAULT NULL,
  `softallocationrule` varchar(20) DEFAULT NULL,
  `allocationrule` varchar(20) DEFAULT NULL,
  `shelflifetype` varchar(1) NOT NULL,
  `outboundlifedays` int(11) NOT NULL,
  `addtime` date NOT NULL,
  `addwho` varchar(35) DEFAULT NULL,
  `edittime` date NOT NULL,
  `editwho` varchar(35) DEFAULT NULL,
  `alternate_sku4` varchar(100) DEFAULT NULL,
  `alternate_sku5` varchar(100) DEFAULT NULL,
  `notes` varchar(4000) DEFAULT NULL,
  `putawayrule` varchar(20) DEFAULT NULL,
  `alternate_sku1` varchar(100) DEFAULT NULL,
  `alternate_sku2` varchar(100) DEFAULT NULL,
  `alternate_sku3` varchar(100) DEFAULT NULL,
  `reservedfield05` varchar(200) DEFAULT NULL,
  `sku_group1` varchar(100) DEFAULT NULL,
  `sku_group2` varchar(100) DEFAULT NULL,
  `sku_group3` varchar(100) DEFAULT NULL,
  `sku_group4` varchar(100) DEFAULT NULL,
  `sku_group5` varchar(100) DEFAULT NULL,
  `reservedfield01` varchar(200) DEFAULT NULL,
  `reservedfield02` varchar(200) DEFAULT NULL,
  `reservedfield03` varchar(200) DEFAULT NULL,
  `reservedfield04` varchar(200) DEFAULT NULL,
  `defaultreceivinguom` varchar(10) DEFAULT NULL,
  `defaultshipmentuom` varchar(10) DEFAULT NULL,
  `defaulthold` varchar(10) DEFAULT NULL,
  `rotationid` varchar(10) DEFAULT NULL,
  `copypackidtolotatt12` varchar(1) DEFAULT 'N',
  `qtymin` decimal(18,3) DEFAULT '0.000',
  `qtymax` decimal(18,3) DEFAULT NULL,
  `shelflife` int(11) DEFAULT '0',
  `hscode` varchar(15) DEFAULT NULL,
  `reservedfield06` varchar(200) DEFAULT NULL,
  `reservedfield07` varchar(200) DEFAULT NULL,
  `reservedfield08` varchar(200) DEFAULT NULL,
  `reservedfield09` varchar(200) DEFAULT NULL,
  `reservedfield10` varchar(200) DEFAULT NULL,
  `reservedfield11` varchar(200) DEFAULT NULL,
  `reservedfield12` varchar(200) DEFAULT NULL,
  `overrcvpercentage` decimal(18,4) DEFAULT NULL,
  `replenishrule` varchar(20) DEFAULT NULL,
  `specialmaintenance` varchar(1) DEFAULT 'N',
  `lastmaintenancedate` date DEFAULT NULL,
  `firstop` varchar(1) DEFAULT 'N',
  `medicaltype` varchar(15) DEFAULT NULL,
  `approvalno` varchar(200) DEFAULT NULL,
  `sn_asn_qty` varchar(1) DEFAULT 'N',
  `sn_so_qty` varchar(1) DEFAULT 'N',
  `invchgwithshipment` varchar(1) DEFAULT 'N',
  `freightclass` varchar(20) DEFAULT NULL,
  `tariffid` varchar(10) DEFAULT NULL,
  `kitflag` varchar(1) NOT NULL DEFAULT 'N',
  `reservecode` varchar(2) DEFAULT NULL,
  `reportuom` varchar(10) NOT NULL DEFAULT 'EA',
  `skulength` decimal(18,4) NOT NULL,
  `skuwidth` decimal(18,4) NOT NULL,
  `skuhigh` decimal(18,4) NOT NULL,
  `qctime` decimal(4,1) DEFAULT NULL,
  `qcrule` varchar(20) DEFAULT NULL,
  `firstinbounddate` date DEFAULT NULL,
  `reservedfield13` varchar(200) DEFAULT NULL,
  `reservedfield14` varchar(200) DEFAULT NULL,
  `reservedfield15` varchar(200) DEFAULT NULL,
  `sku_group6` varchar(100) DEFAULT NULL,
  `sku_group7` varchar(100) DEFAULT NULL,
  `sku_group8` varchar(100) DEFAULT NULL,
  `sku_group9` varchar(100) DEFAULT NULL,
  `medicinespecicalcontrol` char(1) DEFAULT 'N',
  `serialnocatch` char(1) DEFAULT 'N',
  `scanwhencasepicking` char(1) DEFAULT NULL,
  `scanwhenpiecepicking` char(1) DEFAULT 'N',
  `scanwhencheck` char(1) DEFAULT 'N',
  `scanwhenreceive` char(1) DEFAULT 'N',
  `scanwhenputaway` char(1) DEFAULT 'N',
  `shelflifealertdays` int(11) DEFAULT NULL,
  `reservedfield17` varchar(30) DEFAULT NULL,
  `reservedfield18` varchar(30) DEFAULT NULL,
  `reservedfield16` varchar(30) DEFAULT NULL,
  `chk_scn_uom` varchar(10) DEFAULT 'EA',
  `alternative_putawayzone1` varchar(10) DEFAULT NULL,
  `alternative_putawayzone2` varchar(10) DEFAULT NULL,
  `scanwhenpack` char(1) DEFAULT 'N',
  `scanwhenmove` char(1) DEFAULT 'N',
  `imageaddress` varchar(200) DEFAULT NULL,
  `onestepallocation` char(1) DEFAULT 'Y',
  `orderbysql` varchar(100) DEFAULT NULL,
  `secondserialnocatch` char(1) DEFAULT 'N',
  `allowreceiving` char(1) DEFAULT 'Y',
  `allowshipment` char(1) DEFAULT 'Y',
  `inboundserialnoqtycontrol` varchar(20) DEFAULT NULL,
  `outboundserialnoqtycontrol` varchar(20) DEFAULT NULL,
  `defaultcartontype` varchar(10) DEFAULT NULL,
  `breakcs` char(1) DEFAULT 'Y',
  `breakea` char(1) DEFAULT 'Y',
  `breakip` char(1) DEFAULT 'Y',
  `printmedicineqcreport` char(1) DEFAULT 'Y',
  `scanwhenqc` char(1) DEFAULT 'N',
  `overreceiving` char(1) DEFAULT 'N',
  `defaultsupplierid` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`customerid`,`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `bas_location`;
CREATE TABLE `bas_location` (
  `locationid` varchar(20) not null comment '库位编号',
  `length` decimal(18,8) default null,
  `width` decimal(18,8) default null,
  `height` decimal(18,8) default null,
  `locationusage` varchar(2) default null,
  `putawayzone` varchar(10) default null,
  `locationattribute` varchar(2) default null,
  `locationcategory` varchar(2) default null,
  `logicalsequence` varchar(20) default null,
  `locgroup2` varchar(10) default null,
  `cubiccapacity` decimal(18,8) default null,
  `weightcapacity` decimal(18,8) default null,
  `status` varchar(10) default null,
  `facility_id` varchar(10) default null,
  `locgroup1` varchar(10) default null,
  `pickzone` varchar(10) default null,
  `loclevel` varchar(1) default null,
  `xcoord` decimal(65,30) default null,
  `ycoord` decimal(65,30) default null,
  `zcoord` decimal(65,30) default null,
  `create_time` datetime(6) default null,
  `addwho` varchar(35) default null,
  `update_time` datetime(6) not null,
  `editwho` varchar(35) default null,
  `countcapacity` decimal(65,30) default null,
  `environment` varchar(10) default null,
  `demand` varchar(1) default null,
  `mix_flag` varchar(1) default null,
  `loseid_flag` varchar(1) default null,
  `mix_lotflag` varchar(1) default null,
  `locationhandling` varchar(2) default null,
  `plcount` decimal(65,30) default null,
  `picklogicalsequence` varchar(20) default null,
  `cscount` decimal(65,30) default null,
  `xdistance` decimal(65,30) default null,
  `ydistance` decimal(65,30) default null,
  `workingarea` varchar(20) default null,
  `validationcode` varchar(20) default null,
  `wcsaddress1` varchar(50) default null,
  `wcsaddress2` varchar(50) default null,
  `terminalno` varchar(10) default null,
  `skucount` decimal(65,30) default null,
  `sku` varchar(50) default null comment '物料编号',
  `rack_id` varchar(50) default null comment '货架编号',
  `tray_id` varchar(50) default null comment '托盘编号',
  PRIMARY KEY (`LOCATIONID`),
  KEY `IDX_LOCATION_BYLOCATIONUSAGE` (`locationusage`),
  KEY `IDX_LOCATION_BYPUTAWAYZONE` (`putawayzone`,`locationid`),
  KEY `XIE3BAS_LOCATION` (`putawayzone`),
  KEY `XIE4BAS_LOCATION` (`pickzone`),
  KEY `XIE5BAS_LOCATION` (`locgroup1`),
  KEY `XIE6BAS_LOCATION` (`locgroup2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `doc_order_header`;
CREATE TABLE `doc_order_header` (
  `orderno` varchar(20) NOT NULL,
  `customerid` varchar(30) DEFAULT NULL,
  `soreference1` varchar(50) DEFAULT NULL,
  `ordertime` datetime(6) DEFAULT NULL,
  `expectedshipmenttime1` datetime(6) DEFAULT NULL,
  `expectedshipmenttime2` datetime(6) DEFAULT NULL,
  `requireddeliverytime` datetime(6) DEFAULT NULL,
  `priority` varchar(1) DEFAULT NULL,
  `consigneeid` text,
  `c_contact` varchar(20) DEFAULT NULL,
  `consigneename` varchar(200) DEFAULT NULL,
  `c_address1` text,
  `c_address2` varchar(50) DEFAULT NULL,
  `c_address3` varchar(50) DEFAULT NULL,
  `c_address4` varchar(50) DEFAULT NULL,
  `c_city` varchar(50) DEFAULT NULL,
  `c_province` varchar(50) DEFAULT NULL,
  `c_country` varchar(2) DEFAULT NULL,
  `c_fax` varchar(20) DEFAULT NULL,
  `billingid` varchar(30) DEFAULT NULL,
  `c_zip` varchar(100) DEFAULT NULL,
  `b_contact` varchar(200) DEFAULT NULL,
  `billingname` varchar(200) DEFAULT NULL,
  `b_address1` varchar(200) DEFAULT NULL,
  `b_address2` varchar(50) DEFAULT NULL,
  `b_address3` varchar(50) DEFAULT NULL,
  `b_address4` varchar(50) DEFAULT NULL,
  `b_city` varchar(50) DEFAULT NULL,
  `b_province` varchar(50) DEFAULT NULL,
  `b_zip` varchar(10) DEFAULT NULL,
  `b_country` varchar(2) DEFAULT NULL,
  `deliverytermsdescr` varchar(100) DEFAULT NULL,
  `b_fax` varchar(50) DEFAULT NULL,
  `b_email` varchar(100) DEFAULT NULL,
  `paymenttermsdescr` varchar(100) DEFAULT NULL,
  `deliveryterms` varchar(10) DEFAULT NULL,
  `paymentterms` varchar(10) DEFAULT NULL,
  `door` varchar(10) DEFAULT NULL,
  `route` varchar(10) DEFAULT NULL,
  `stop` varchar(10) DEFAULT NULL,
  `sostatus` varchar(2) DEFAULT NULL,
  `placeofdischarge` varchar(60) DEFAULT NULL,
  `placeofdelivery` varchar(60) DEFAULT NULL,
  `ordertype` varchar(20) DEFAULT NULL,
  `userdefine1` varchar(200) DEFAULT NULL,
  `userdefine2` varchar(200) DEFAULT NULL,
  `userdefine3` varchar(200) DEFAULT NULL,
  `userdefine4` varchar(200) DEFAULT NULL,
  `userdefine5` varchar(200) DEFAULT NULL,
  `create_time` datetime(6) NOT NULL,
  `addwho` varchar(35) DEFAULT NULL,
  `update_time` datetime(6) NOT NULL,
  `editwho` varchar(35) DEFAULT NULL,
  `notes` text,
  `soreference2` varchar(50) DEFAULT NULL,
  `soreference3` varchar(50) DEFAULT NULL,
  `carrierid` varchar(30) DEFAULT NULL,
  `carriername` varchar(200) DEFAULT NULL,
  `carrieraddress1` varchar(200) DEFAULT NULL,
  `carrieraddress3` varchar(50) DEFAULT NULL,
  `carrieraddress2` varchar(50) DEFAULT NULL,
  `carrieraddress4` varchar(50) DEFAULT NULL,
  `carriercity` varchar(50) DEFAULT NULL,
  `carrierprovince` varchar(50) DEFAULT NULL,
  `carriercountry` varchar(20) DEFAULT NULL,
  `carrierzip` varchar(10) DEFAULT NULL,
  `issuepartyid` varchar(30) DEFAULT NULL,
  `issuepartyname` varchar(200) DEFAULT NULL,
  `i_address1` varchar(200) DEFAULT NULL,
  `i_address2` varchar(100) DEFAULT NULL,
  `i_address3` varchar(100) DEFAULT NULL,
  `i_address4` varchar(100) DEFAULT NULL,
  `i_city` varchar(100) DEFAULT NULL,
  `i_province` varchar(100) DEFAULT NULL,
  `i_country` varchar(2) DEFAULT NULL,
  `i_zip` varchar(100) DEFAULT NULL,
  `lastshipmenttime` datetime(6) DEFAULT NULL,
  `edisendflag` char(1) DEFAULT NULL,
  `picking_print_flag` varchar(1) DEFAULT NULL,
  `createsource` varchar(35) DEFAULT NULL,
  `edisendtime` datetime(6) DEFAULT NULL,
  `edisendtime2` datetime(6) DEFAULT NULL,
  `edisendtime3` datetime(6) DEFAULT NULL,
  `edisendtime4` datetime(6) DEFAULT NULL,
  `edisendtime5` datetime(6) DEFAULT NULL,
  `b_tel1` varchar(50) DEFAULT NULL,
  `b_tel2` varchar(50) DEFAULT NULL,
  `carriercontact` varchar(200) DEFAULT NULL,
  `carriermail` varchar(50) DEFAULT NULL,
  `carrierfax` varchar(50) DEFAULT NULL,
  `carriertel1` varchar(50) DEFAULT NULL,
  `carriertel2` varchar(40) DEFAULT NULL,
  `c_mail` varchar(100) DEFAULT NULL,
  `c_tel1` varchar(100) DEFAULT NULL,
  `c_tel2` varchar(100) DEFAULT NULL,
  `i_contact` varchar(20) DEFAULT NULL,
  `i_mail` varchar(100) DEFAULT NULL,
  `i_fax` varchar(100) DEFAULT NULL,
  `i_tel1` varchar(100) DEFAULT NULL,
  `i_tel2` varchar(100) DEFAULT NULL,
  `releasestatus` varchar(1) DEFAULT NULL,
  `transportation` varchar(100) DEFAULT NULL,
  `soreference4` varchar(100) DEFAULT NULL,
  `soreference5` varchar(50) DEFAULT NULL,
  `h_edi_01` varchar(200) DEFAULT NULL COMMENT '订单总价',
  `h_edi_02` varchar(200) DEFAULT NULL COMMENT '增值税客户税号',
  `h_edi_03` varchar(200) DEFAULT NULL COMMENT '增值税发票地址电话',
  `h_edi_04` varchar(200) DEFAULT NULL COMMENT '运费',
  `h_edi_05` varchar(200) DEFAULT NULL COMMENT 'OMS的追加备注，记录增值税发票的实际快递地址',
  `h_edi_06` varchar(200) DEFAULT NULL COMMENT '增值税发票银行账号',
  `h_edi_07` varchar(200) DEFAULT NULL COMMENT '异常订单标记',
  `h_edi_08` text COMMENT '客服备注',
  `h_edi_09` decimal(18,8) DEFAULT NULL,
  `h_edi_10` decimal(18,8) DEFAULT NULL,
  `userdefine6` varchar(200) DEFAULT NULL,
  `order_print_flag` varchar(1) DEFAULT NULL,
  `rfgettask` varchar(1) DEFAULT NULL,
  `warehouseid` varchar(30) DEFAULT NULL,
  `erpcancelflag` char(1) DEFAULT NULL,
  `zonegroup` varchar(10) DEFAULT NULL,
  `medicalxmltime` datetime(6) DEFAULT NULL,
  `placeofloading` varchar(60) DEFAULT NULL,
  `requiredeliveryno` char(1) DEFAULT NULL,
  `singlematch` varchar(200) DEFAULT NULL,
  `serialnocatch` char(1) DEFAULT NULL,
  `followup` varchar(20) DEFAULT NULL,
  `userdefinea` varchar(20) DEFAULT NULL,
  `userdefineb` varchar(20) DEFAULT NULL,
  `salesorderno` varchar(20) DEFAULT NULL,
  `invoiceprintflag` char(1) DEFAULT NULL,
  `invoiceno` varchar(100) DEFAULT NULL,
  `invoicetitle` text,
  `invoicetype` text,
  `invoiceitem` text,
  `invoiceamount` decimal(10,2) DEFAULT NULL,
  `archiveflag` char(1) DEFAULT NULL,
  `consigneename_e` varchar(200) DEFAULT NULL,
  `puttolocation` varchar(20) DEFAULT NULL,
  `ful_alc` char(1) DEFAULT NULL,
  `deliveryno` varchar(30) DEFAULT NULL,
  `channel` varchar(20) DEFAULT NULL,
  `waveno` varchar(30) DEFAULT NULL,
  `allocationcount` decimal(65,30) DEFAULT NULL,
  `expressprintflag` char(1) DEFAULT NULL,
  `deliverynoteprintflag` char(1) DEFAULT NULL,
  `weightingflag` char(1) DEFAULT NULL,
  `udfprintflag1` char(1) DEFAULT NULL,
  `udfprintflag2` char(1) DEFAULT NULL,
  `udfprintflag3` char(1) DEFAULT NULL,
  `edisendflag3` char(1) DEFAULT NULL,
  `edisendflag2` char(1) DEFAULT NULL,
  `soreference6` varchar(30) DEFAULT NULL,
  `sign_day` varchar(30) DEFAULT NULL,
  `h_edi_11` varchar(30) DEFAULT NULL,
  `h_edi_12` varchar(30) DEFAULT NULL,
  `h_edi_13` varchar(30) DEFAULT NULL COMMENT '增值税发票导出标记',
  `h_edi_14` varchar(30) DEFAULT NULL COMMENT '顺丰获取面单号接口打标记',
  `h_edi_15` varchar(30) DEFAULT NULL COMMENT 'EMS和圆通接口回传标记打印',
  PRIMARY KEY (`orderno`),
  KEY `idx_doc_order_header_p` (`allocationcount`,`priority`,`sostatus`) USING BTREE,
  KEY `i_doc_order_details_time` (`expectedshipmenttime1`,`customerid`) USING BTREE,
  KEY `i_doc_order_header_archiveflag` (`archiveflag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for doc_order_details
-- ----------------------------
DROP TABLE IF EXISTS `doc_order_details`;
CREATE TABLE `doc_order_details` (
  `orderno` varchar(20) NOT NULL,
  `orderlineno` varchar(20) NOT NULL,
  `customerid` varchar(30) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `linestatus` varchar(2) DEFAULT NULL,
  `lotnum` varchar(10) DEFAULT NULL,
  `lotatt01` varchar(20) DEFAULT NULL,
  `lotatt02` varchar(20) DEFAULT NULL,
  `lotatt03` varchar(20) DEFAULT NULL,
  `lotatt04` varchar(100) DEFAULT NULL,
  `lotatt05` varchar(100) DEFAULT NULL,
  `lotatt06` varchar(100) DEFAULT NULL,
  `lotatt07` varchar(100) DEFAULT NULL,
  `lotatt08` varchar(100) DEFAULT NULL,
  `lotatt09` varchar(100) DEFAULT NULL,
  `lotatt10` varchar(100) DEFAULT NULL,
  `lotatt11` varchar(100) DEFAULT NULL,
  `lotatt12` varchar(100) DEFAULT NULL,
  `pickzone` varchar(10) DEFAULT NULL,
  `location` varchar(20) DEFAULT NULL,
  `traceid` varchar(30) DEFAULT NULL,
  `qtyordered` decimal(18,8) DEFAULT NULL,
  `qtysoftallocated` decimal(18,8) DEFAULT NULL,
  `qtyallocated` decimal(18,8) DEFAULT NULL,
  `qtypicked` decimal(18,8) DEFAULT NULL,
  `qtyshipped` decimal(18,8) DEFAULT NULL,
  `uom` varchar(10) DEFAULT NULL,
  `packid` varchar(40) DEFAULT NULL,
  `softallocationrule` varchar(20) DEFAULT NULL,
  `allocationrule` varchar(20) DEFAULT NULL,
  `userdefine1` varchar(200) DEFAULT NULL,
  `userdefine2` varchar(200) DEFAULT NULL,
  `userdefine3` varchar(200) DEFAULT NULL,
  `userdefine4` varchar(200) DEFAULT NULL,
  `userdefine5` varchar(200) DEFAULT NULL,
  `notes` varchar(100) DEFAULT NULL,
  `qtyordered_each` decimal(18,8) DEFAULT NULL,
  `qtysoftallocated_each` decimal(18,8) DEFAULT NULL,
  `qtyallocated_each` decimal(18,8) DEFAULT NULL,
  `qtypicked_each` decimal(18,8) DEFAULT NULL,
  `qtyshipped_each` decimal(18,8) DEFAULT NULL,
  `create_time` datetime(6) DEFAULT NULL,
  `addwho` varchar(35) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `editwho` varchar(35) DEFAULT NULL,
  `rotationid` varchar(10) DEFAULT NULL,
  `netweight` decimal(18,8) DEFAULT NULL,
  `grossweight` decimal(18,8) DEFAULT NULL,
  `cubic` decimal(18,8) DEFAULT NULL,
  `price` decimal(18,8) DEFAULT NULL,
  `alternativesku` varchar(100) DEFAULT NULL,
  `kitreferenceno` decimal(65,30) DEFAULT NULL,
  `d_edi_01` varchar(200) DEFAULT NULL,
  `d_edi_02` varchar(200) DEFAULT NULL,
  `d_edi_03` varchar(200) DEFAULT NULL,
  `d_edi_04` varchar(200) DEFAULT NULL,
  `d_edi_05` varchar(200) DEFAULT NULL,
  `d_edi_06` varchar(200) DEFAULT NULL,
  `d_edi_07` varchar(200) DEFAULT NULL,
  `d_edi_08` varchar(200) DEFAULT NULL,
  `d_edi_09` decimal(18,8) DEFAULT NULL,
  `d_edi_10` decimal(18,8) DEFAULT NULL,
  `orderlinereferenceno` varchar(30) DEFAULT NULL,
  `d_edi_11` varchar(200) DEFAULT NULL,
  `d_edi_12` varchar(200) DEFAULT NULL,
  `d_edi_13` varchar(200) DEFAULT NULL,
  `d_edi_14` varchar(200) DEFAULT NULL,
  `d_edi_15` varchar(200) DEFAULT NULL,
  `d_edi_16` varchar(200) DEFAULT NULL,
  `d_edi_17` varchar(200) DEFAULT NULL,
  `d_edi_18` varchar(200) DEFAULT NULL,
  `d_edi_19` varchar(200) DEFAULT NULL,
  `d_edi_20` varchar(200) DEFAULT NULL,
  `kitsku` varchar(50) DEFAULT NULL,
  `erpcancelflag` char(1) DEFAULT NULL,
  `userdefine6` varchar(200) DEFAULT NULL,
  `zonegroup` varchar(10) DEFAULT NULL,
  `locgroup1` varchar(10) DEFAULT NULL,
  `locgroup2` varchar(10) DEFAULT NULL,
  `comminglesku` char(1) DEFAULT NULL,
  `onestepallocation` char(1) DEFAULT NULL,
  `orderlotcontrol` char(1) DEFAULT NULL,
  `fullcaselotcontrol` char(1) DEFAULT NULL,
  `piecelotcontrol` char(1) DEFAULT NULL,
  `referencelineno` decimal(65,30) DEFAULT NULL,
  `salesorderno` varchar(20) DEFAULT NULL,
  `salesorderlineno` varchar(20) DEFAULT NULL,
  `qtyreleased` decimal(18,8) DEFAULT NULL,
  `freegift` char(1) DEFAULT NULL,
  PRIMARY KEY (`orderno`,`orderlineno`),
  KEY `idx_doc_order_details_3` (`orderno`,`qtyordered_each`) USING BTREE,
  KEY `idx_doc_order_details_status` (`linestatus`) USING BTREE,
  KEY `i_doc_order_details_3` (`orderno`,`linestatus`) USING BTREE,
  KEY `i_doc_order_details_a` (`customerid`,`sku`,`pickzone`,`location`,`traceid`) USING BTREE,
  KEY `i_doc_order_details_oloa` (`orderno`,`linestatus`,`qtyordered_each`,`qtyallocated_each`) USING BTREE,
  KEY `i_doc_order_details_ss` (`salesorderno`,`salesorderlineno`) USING BTREE,
  KEY `xie1doc_order_details` (`orderno`,`customerid`,`sku`) USING BTREE,
  KEY `xie2doc_order_details` (`packid`,`uom`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for doc_asn_header 入库
-- ----------------------------
DROP TABLE IF EXISTS `doc_asn_header`;
CREATE TABLE `doc_asn_header` (
  `asnno` varchar(20) NOT NULL,
  `asncreationtime` date NOT NULL,
  `carrierid` varchar(30) DEFAULT NULL,
  `asnreference1` varchar(50) DEFAULT NULL,
  `carriername` varchar(200) DEFAULT NULL,
  `expectedarrivetime1` date DEFAULT NULL,
  `expectedarrivetime2` date DEFAULT NULL,
  `carrieraddress1` varchar(200) DEFAULT NULL,
  `carrieraddress3` varchar(100) DEFAULT NULL,
  `asnreference3` varchar(50) DEFAULT NULL,
  `carrieraddress2` varchar(100) DEFAULT NULL,
  `carrieraddress4` varchar(100) DEFAULT NULL,
  `door` varchar(50) DEFAULT NULL,
  `carriercity` varchar(50) DEFAULT NULL,
  `carrierprovince` varchar(50) DEFAULT NULL,
  `carrierzip` varchar(10) DEFAULT NULL,
  `asnreference4` varchar(100) DEFAULT NULL,
  `countryoforigin` varchar(2) DEFAULT NULL,
  `countryofdestination` varchar(2) DEFAULT NULL,
  `deliveryvehicleno` varchar(20) DEFAULT NULL,
  `carriercountry` varchar(20) DEFAULT NULL,
  `asnreference5` varchar(50) DEFAULT NULL,
  `placeofloading` varchar(60) DEFAULT NULL,
  `placeofdischarge` varchar(60) DEFAULT NULL,
  `placeofdelivery` varchar(60) DEFAULT NULL,
  `paymentterms` varchar(4) DEFAULT NULL,
  `paymenttermsdescr` varchar(100) DEFAULT NULL,
  `userdefine5` varchar(200) DEFAULT NULL,
  `asnstatus` varchar(2) NOT NULL,
  `userdefine1` varchar(200) DEFAULT NULL,
  `userdefine2` varchar(200) DEFAULT NULL,
  `userdefine3` varchar(200) DEFAULT NULL,
  `userdefine4` varchar(200) DEFAULT NULL,
  `asntype` varchar(20) DEFAULT NULL,
  `asnreference2` varchar(50) DEFAULT NULL,
  `create_time` datetime(6) NOT NULL,
  `pono` varchar(20) DEFAULT NULL,
  `addwho` varchar(35) DEFAULT NULL,
  `update_time` datetime(6) NOT NULL,
  `editwho` varchar(35) DEFAULT NULL,
  `deliveryterms` varchar(10) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `deliverytermsdescr` varchar(100) DEFAULT NULL,
  `customerid` varchar(30) NOT NULL,
  `createsource` varchar(35) DEFAULT NULL,
  `bytrace_flag` varchar(1) DEFAULT 'N',
  `supplierid` varchar(30) DEFAULT NULL,
  `reserve_flag` varchar(1) NOT NULL DEFAULT 'N',
  `edisendtime` date DEFAULT NULL,
  `receiveid` float DEFAULT NULL,
  `supplier_name` varchar(200) DEFAULT NULL,
  `supplier_address1` varchar(200) DEFAULT NULL,
  `supplier_address2` varchar(100) DEFAULT NULL,
  `supplier_address3` varchar(100) DEFAULT NULL,
  `supplier_address4` varchar(100) DEFAULT NULL,
  `supplier_city` varchar(50) DEFAULT NULL,
  `supplier_province` varchar(50) DEFAULT NULL,
  `supplier_zip` varchar(10) DEFAULT NULL,
  `supplier_country` varchar(10) DEFAULT NULL,
  `supplier_tel1` varchar(50) DEFAULT NULL,
  `supplier_fax` varchar(50) DEFAULT NULL,
  `supplier_tel2` varchar(40) DEFAULT NULL,
  `issuepartyid` varchar(30) DEFAULT NULL,
  `issuepartyname` varchar(200) DEFAULT NULL,
  `i_address1` varchar(200) DEFAULT NULL,
  `i_address2` varchar(100) DEFAULT NULL,
  `i_address3` varchar(100) DEFAULT NULL,
  `i_address4` varchar(100) DEFAULT NULL,
  `i_city` varchar(50) DEFAULT NULL,
  `i_province` varchar(50) DEFAULT NULL,
  `i_country` varchar(2) DEFAULT NULL,
  `i_zip` varchar(10) DEFAULT NULL,
  `deliveryvehicletype` varchar(10) DEFAULT NULL,
  `lastreceivingtime` date DEFAULT NULL,
  `edisendflag` char(1) NOT NULL DEFAULT 'N',
  `billingid` varchar(30) DEFAULT NULL,
  `billingname` varchar(200) DEFAULT NULL,
  `b_address1` varchar(200) DEFAULT NULL,
  `b_address2` varchar(100) DEFAULT NULL,
  `b_address3` varchar(100) DEFAULT NULL,
  `b_address4` varchar(100) DEFAULT NULL,
  `b_city` varchar(50) DEFAULT NULL,
  `b_province` varchar(50) DEFAULT NULL,
  `b_country` varchar(2) DEFAULT NULL,
  `b_zip` varchar(10) DEFAULT NULL,
  `billingclass_group` varchar(10) DEFAULT NULL,
  `driver` varchar(50) DEFAULT NULL,
  `carriercontact` varchar(200) DEFAULT NULL,
  `carriertel1` varchar(50) DEFAULT NULL,
  `supplier_contact` varchar(200) DEFAULT NULL,
  `supplier_mail` varchar(100) DEFAULT NULL,
  `carriertel2` varchar(40) DEFAULT NULL,
  `carrierfax` varchar(50) DEFAULT NULL,
  `carriermail` varchar(100) DEFAULT NULL,
  `edisendtime2` date DEFAULT NULL,
  `edisendtime3` date DEFAULT NULL,
  `edisendtime4` date DEFAULT NULL,
  `edisendtime5` date DEFAULT NULL,
  `b_contact` varchar(20) DEFAULT NULL,
  `b_mail` varchar(100) DEFAULT NULL,
  `b_fax` varchar(20) DEFAULT NULL,
  `b_tel1` varchar(40) DEFAULT NULL,
  `b_tel2` varchar(40) DEFAULT NULL,
  `i_contact` varchar(20) DEFAULT NULL,
  `i_mail` varchar(100) DEFAULT NULL,
  `i_fax` varchar(20) DEFAULT NULL,
  `i_tel1` varchar(40) DEFAULT NULL,
  `i_tel2` varchar(40) DEFAULT NULL,
  `h_edi_01` varchar(200) DEFAULT NULL,
  `h_edi_02` varchar(200) DEFAULT NULL,
  `h_edi_03` varchar(200) DEFAULT NULL,
  `h_edi_04` varchar(200) DEFAULT NULL,
  `h_edi_05` varchar(200) DEFAULT NULL,
  `h_edi_06` varchar(200) DEFAULT NULL,
  `h_edi_07` varchar(200) DEFAULT NULL,
  `h_edi_08` varchar(200) DEFAULT NULL,
  `h_edi_09` decimal(18,8) DEFAULT NULL,
  `h_edi_10` decimal(18,8) DEFAULT NULL,
  `userdefine6` varchar(200) DEFAULT NULL,
  `asn_print_flag` varchar(1) DEFAULT 'N',
  `qcstatus` varchar(2) DEFAULT NULL,
  `return_print_flag` char(1) DEFAULT 'N',
  `warehouseid` varchar(30) DEFAULT NULL,
  `priority` char(1) DEFAULT NULL,
  `zonegroup` varchar(10) DEFAULT NULL,
  `releasestatus` char(1) DEFAULT NULL,
  `packmaterialconsume` char(1) DEFAULT NULL,
  `medicalxmltime` date DEFAULT NULL,
  `serialnocatch` char(1) DEFAULT 'Y',
  `followup` varchar(20) DEFAULT NULL,
  `userdefinea` varchar(20) DEFAULT NULL,
  `userdefineb` varchar(20) DEFAULT NULL,
  `archiveflag` char(1) NOT NULL DEFAULT 'N',
  `actualarrivetime` date DEFAULT NULL,
  `h_edi_11` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`asnno`),
  KEY `idx_doc_asn_header_customerid` (`customerid`,`asnstatus`) USING BTREE,
  KEY `idx_doc_asn_header_ref1` (`asnreference1`) USING BTREE,
  KEY `i_doc_asn_header_archiveflag` (`archiveflag`) USING BTREE,
  KEY `i_doc_asn_header_asns` (`asnstatus`) USING BTREE,
  KEY `i_doc_asn_header_edi` (`customerid`,`asnstatus`,`edisendtime`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for doc_asn_details
-- ----------------------------
DROP TABLE IF EXISTS `doc_asn_details`;
CREATE TABLE `doc_asn_details` (
  `asnno` varchar(20) NOT NULL,
  `asnlineno` varchar(20) NOT NULL,
  `customerid` varchar(30) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `skudescrc` varchar(200) DEFAULT NULL,
  `skudescre` varchar(200) DEFAULT NULL,
  `pono` varchar(20) DEFAULT NULL,
  `linestatus` varchar(2) DEFAULT NULL,
  `receivedtime` date DEFAULT NULL,
  `expectedqty` decimal(18,8) DEFAULT NULL,
  `expectedqty_each` decimal(18,8) DEFAULT NULL,
  `rejectedqty` decimal(18,8) DEFAULT NULL,
  `rejectedqty_each` decimal(18,8) DEFAULT NULL,
  `receivedqty` decimal(18,8) DEFAULT NULL,
  `receivedqty_each` decimal(18,8) DEFAULT NULL,
  `uom` varchar(10) DEFAULT NULL,
  `packid` varchar(40) DEFAULT NULL,
  `holdrejectcode` varchar(2) DEFAULT NULL,
  `holdrejectreason` varchar(60) DEFAULT NULL,
  `containerid` varchar(30) DEFAULT NULL,
  `receivinglocation` varchar(20) DEFAULT NULL,
  `lotatt01` varchar(20) DEFAULT NULL,
  `lotatt02` varchar(20) DEFAULT NULL,
  `lotatt03` varchar(20) DEFAULT NULL,
  `lotatt04` varchar(100) DEFAULT NULL,
  `lotatt05` varchar(100) DEFAULT NULL,
  `lotatt06` varchar(100) DEFAULT NULL,
  `lotatt07` varchar(100) DEFAULT NULL,
  `lotatt08` varchar(100) DEFAULT NULL,
  `lotatt09` varchar(100) DEFAULT NULL,
  `lotatt10` varchar(100) DEFAULT NULL,
  `lotatt11` varchar(100) DEFAULT NULL,
  `lotatt12` varchar(100) DEFAULT NULL,
  `lotatt13` varchar(100) DEFAULT NULL,
  `lotatt14` varchar(100) DEFAULT NULL,
  `lotatt15` varchar(100) DEFAULT NULL,
  `lotatt16` varchar(100) DEFAULT NULL,
  `lotatt17` varchar(100) DEFAULT NULL,
  `lotatt18` varchar(100) DEFAULT NULL,
  `productstatus` varchar(2) DEFAULT NULL,
  `productstatus_descr` varchar(60) DEFAULT NULL,
  `totalcubic` decimal(18,8) DEFAULT NULL,
  `totalgrossweight` decimal(18,8) DEFAULT NULL,
  `totalnetweight` decimal(18,8) DEFAULT NULL,
  `totalprice` decimal(18,4) DEFAULT NULL,
  `userdefine1` varchar(200) DEFAULT NULL,
  `userdefine2` varchar(200) DEFAULT NULL,
  `userdefine3` varchar(200) DEFAULT NULL,
  `userdefine4` varchar(200) DEFAULT NULL,
  `userdefine5` varchar(200) DEFAULT NULL,
  `notes` varchar(4000) DEFAULT NULL,
  `create_time` date NOT NULL,
  `addwho` varchar(35) DEFAULT NULL,
  `update_time` date NOT NULL,
  `editwho` varchar(35) DEFAULT NULL,
  `createsource` varchar(35) DEFAULT NULL,
  `palletizeqty_each` decimal(18,8) DEFAULT NULL,
  `palletizemethod` varchar(8) DEFAULT NULL,
  `plantoloc` varchar(20) DEFAULT NULL,
  `polineno` int(11) DEFAULT NULL,
  `reserve_flag` varchar(1) DEFAULT 'N',
  `d_edi_01` varchar(200) DEFAULT NULL,
  `d_edi_02` varchar(200) DEFAULT NULL,
  `d_edi_03` varchar(200) DEFAULT NULL,
  `d_edi_04` varchar(200) DEFAULT NULL,
  `d_edi_05` varchar(200) DEFAULT NULL,
  `d_edi_06` varchar(200) DEFAULT NULL,
  `d_edi_07` varchar(200) DEFAULT NULL,
  `d_edi_08` varchar(200) DEFAULT NULL,
  `alternativesku` varchar(100) DEFAULT NULL,
  `alternativedescr_c` varchar(200) DEFAULT NULL,
  `printlabel` varchar(1) DEFAULT NULL,
  `damagedqty_each` decimal(18,8) DEFAULT NULL,
  `d_edi_09` decimal(18,8) DEFAULT NULL,
  `d_edi_10` decimal(18,8) DEFAULT NULL,
  `qcstatus` varchar(2) DEFAULT NULL,
  `d_edi_11` varchar(200) DEFAULT NULL,
  `d_edi_12` varchar(200) DEFAULT NULL,
  `d_edi_13` varchar(200) DEFAULT NULL,
  `d_edi_14` varchar(200) DEFAULT NULL,
  `d_edi_15` varchar(200) DEFAULT NULL,
  `d_edi_16` varchar(200) DEFAULT NULL,
  `d_edi_17` varchar(200) DEFAULT NULL,
  `d_edi_18` varchar(200) DEFAULT NULL,
  `d_edi_19` varchar(200) DEFAULT NULL,
  `d_edi_20` varchar(200) DEFAULT NULL,
  `userdefine6` varchar(200) DEFAULT NULL,
  `prereceivedqty_each` decimal(18,8) DEFAULT '0.00000000',
  `overrcvpercentage` decimal(18,8) DEFAULT NULL,
  `referencelineno` int(11) DEFAULT NULL,
  `asnlinefilter` varchar(100) DEFAULT NULL,
  `operator` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`asnno`,`asnlineno`),
  KEY `idx_doc_asn_details_2` (`asnno`,`expectedqty_each`) USING BTREE,
  KEY `xie1doc_asn_details` (`pono`,`polineno`) USING BTREE,
  KEY `xie2doc_asn_details` (`customerid`,`sku`,`asnno`) USING BTREE,
  KEY `xie3doc_asn_details` (`packid`,`uom`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `inv_lot_loc_id`;
CREATE TABLE `inv_lot_loc_id` (
  `rackid` varchar(20) NOT NULL,
  `locationid` varchar(20) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `lotnum` varchar(10) NOT NULL,
  `traceid` varchar(30) DEFAULT NULL,
  `customerid` varchar(30) NOT NULL,
  `qty` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `qtyallocated` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `qtyrpin` decimal(18,3) DEFAULT NULL,
  `qtyrpout` decimal(18,3) DEFAULT NULL,
  `qtymvin` decimal(18,3) DEFAULT NULL,
  `qtymvout` decimal(19,4) DEFAULT NULL,
  `qtyonhold` decimal(18,3) DEFAULT NULL,
  `addwho` varchar(35) DEFAULT NULL,
  `editwho` varchar(35) DEFAULT NULL,
  `netweight` decimal(18,8) DEFAULT NULL,
  `grossweight` decimal(18,8) DEFAULT NULL,
  `cubic` decimal(18,8) DEFAULT NULL,
  `price` decimal(18,8) DEFAULT NULL,
  `onholdlocker` int(11) DEFAULT '0',
  `lpn` varchar(30) DEFAULT NULL,
  `qtypa` decimal(18,8) DEFAULT '0.00000000',
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`rackid`,`locationid`) USING BTREE,
  KEY `idx_inv_lot_loc_id_c` (`locationid`,`sku`) USING BTREE,
  KEY `idx_inv_lot_loc_id_qqqqq` (`qty`,`qtyallocated`,`qtyrpin`,`qtymvin`,`qtypa`) USING BTREE,
  KEY `idx_lotxlocxid_sku` (`sku`) USING BTREE,
  KEY `i_inv_lot_loc_id_a` (`customerid`,`sku`) USING BTREE,
  KEY `i_inv_lot_loc_id_b` (`traceid`) USING BTREE,
  KEY `i_inv_lot_loc_id_c` (`qty`,`qtyrpin`,`qtymvin`) USING BTREE,
  KEY `i_inv_lot_loc_id_lot` (`lotnum`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ENTRANCE_PRIVILEGE
-- ----------------------------

DROP TABLE IF EXISTS `entrance_privilege`;
CREATE TABLE `entrance_privilege` (
  `entranceid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(20) NOT NULL,
  `orderno` varchar(20) NOT NULL,
  `entrancestatus` varchar(2) DEFAULT NULL,
  `createtime` date NOT NULL,
  `updatetime` date NOT NULL,
  `entertime` date DEFAULT NULL,
  `leavetime` date DEFAULT NULL,
  PRIMARY KEY (`entranceid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_BLOB_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_CALENDARS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_CRON_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_FIRED_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_JOB_DETAILS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_LOCKS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_SCHEDULER_STATE
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for QRTZ_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `QRTZ_JOB_DETAILS` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新日期',
  `version` bigint(20) NOT NULL DEFAULT '0',
  `orders` int(11) DEFAULT NULL,
  `full_name` longtext NOT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tree_path` varchar(255) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ind_area_parent` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地区';

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务日志';

-- ----------------------------
-- Table structure for sn
-- ----------------------------
DROP TABLE IF EXISTS `sn`;
CREATE TABLE `sn` (
  `type` int(11) NOT NULL COMMENT '类型',
  `last_value` bigint(20) NOT NULL COMMENT '末值',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改日期',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='序列号';

-- ----------------------------
-- Table structure for sys_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha` (
  `uuid` char(36) NOT NULL COMMENT 'uuid',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统验证码';

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `param_key` (`param_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统配置信息表';

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统日志';

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件上传';

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) DEFAULT NULL COMMENT '盐',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户Token';

-- 初始数据
INSERT INTO `sys_user` (`user_id`, `username`, `password`, `salt`, `email`, `mobile`, `status`, `create_user_id`, `create_time`) VALUES ('1', 'admin', '9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d', 'YzcmCZNvbXocrsz9dm8e', 'root@sdb.io', '13612345678', '1', '1', '2016-11-11 11:11:11');

INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (1, 0, '系统管理', NULL, NULL, 0, 'system', 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (2, 1, '管理员列表', 'sys/user', NULL, 1, 'admin', 1);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (3, 1, '角色管理', 'sys/role', NULL, 1, 'role', 2);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (4, 1, '菜单管理', 'sys/menu', NULL, 1, 'menu', 3);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (5, 1, 'SQL监控', 'http://localhost:8080/sdb/druid/sql.html', NULL, 1, 'sql', 4);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (6, 1, '定时任务', 'job/schedule', NULL, 1, 'job', 5);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (7, 6, '查看', NULL, 'sys:schedule:list,sys:schedule:info', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (8, 6, '新增', NULL, 'sys:schedule:save', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (9, 6, '修改', NULL, 'sys:schedule:update', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (10, 6, '删除', NULL, 'sys:schedule:delete', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (11, 6, '暂停', NULL, 'sys:schedule:pause', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (12, 6, '恢复', NULL, 'sys:schedule:resume', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (13, 6, '立即执行', NULL, 'sys:schedule:run', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (14, 6, '日志列表', NULL, 'sys:schedule:log', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (27, 1, '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'config', 6);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (29, 1, '系统日志', 'sys/log', 'sys:log:list', 1, 'log', 7);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (30, 1, '文件上传', 'oss/oss', 'sys:oss:all', 1, 'oss', 6);

INSERT INTO `sys_config` (`param_key`, `param_value`, `status`, `remark`) VALUES ('CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', '0', '云存储配置信息');

INSERT INTO `sn`(`type`, `last_value`, `create_date`, `update_date`) VALUES (1, 100, '2018-05-23 14:52:55', '2018-05-23 14:52:55');
INSERT INTO `wareams`.`sn`(`type`, `last_value`, `create_date`, `update_date`) VALUES (2, 100, '2019-07-20 15:31:20', '2019-07-20 15:31:20');
INSERT INTO `wareams`.`sn`(`type`, `last_value`, `create_date`, `update_date`) VALUES (3, 100, '2019-07-20 15:31:20', '2019-07-20 15:31:20');
INSERT INTO `wareams`.`sn`(`type`, `last_value`, `create_date`, `update_date`) VALUES (4, 100, '2019-07-23 23:08:39', '2019-07-23 23:08:39');