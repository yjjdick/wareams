
package bcssg.wdssvr.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>deviceStatusEntity complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="deviceStatusEntity"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DEVICE_ABN_INFO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEVICE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEVICE_STATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEVICE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RESERVE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RESERVE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RESERVE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RESERVE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TIMESTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WAREHOUSE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deviceStatusEntity", propOrder = {
    "deviceabninfo",
    "devicecode",
    "devicestatus",
    "devicetype",
    "reserve1",
    "reserve2",
    "reserve3",
    "reserve4",
    "timestamp",
    "warehousecode"
})
public class DeviceStatusEntity {

    @XmlElement(name = "DEVICE_ABN_INFO")
    protected String deviceabninfo;
    @XmlElement(name = "DEVICE_CODE")
    protected String devicecode;
    @XmlElement(name = "DEVICE_STATUS")
    protected String devicestatus;
    @XmlElement(name = "DEVICE_TYPE")
    protected String devicetype;
    @XmlElement(name = "RESERVE1")
    protected String reserve1;
    @XmlElement(name = "RESERVE2")
    protected String reserve2;
    @XmlElement(name = "RESERVE3")
    protected String reserve3;
    @XmlElement(name = "RESERVE4")
    protected String reserve4;
    @XmlElement(name = "TIMESTAMP")
    protected String timestamp;
    @XmlElement(name = "WAREHOUSE_CODE")
    protected String warehousecode;

    /**
     * 获取deviceabninfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEVICEABNINFO() {
        return deviceabninfo;
    }

    /**
     * 设置deviceabninfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEVICEABNINFO(String value) {
        this.deviceabninfo = value;
    }

    /**
     * 获取devicecode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEVICECODE() {
        return devicecode;
    }

    /**
     * 设置devicecode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEVICECODE(String value) {
        this.devicecode = value;
    }

    /**
     * 获取devicestatus属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEVICESTATUS() {
        return devicestatus;
    }

    /**
     * 设置devicestatus属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEVICESTATUS(String value) {
        this.devicestatus = value;
    }

    /**
     * 获取devicetype属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEVICETYPE() {
        return devicetype;
    }

    /**
     * 设置devicetype属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEVICETYPE(String value) {
        this.devicetype = value;
    }

    /**
     * 获取reserve1属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESERVE1() {
        return reserve1;
    }

    /**
     * 设置reserve1属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESERVE1(String value) {
        this.reserve1 = value;
    }

    /**
     * 获取reserve2属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESERVE2() {
        return reserve2;
    }

    /**
     * 设置reserve2属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESERVE2(String value) {
        this.reserve2 = value;
    }

    /**
     * 获取reserve3属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESERVE3() {
        return reserve3;
    }

    /**
     * 设置reserve3属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESERVE3(String value) {
        this.reserve3 = value;
    }

    /**
     * 获取reserve4属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESERVE4() {
        return reserve4;
    }

    /**
     * 设置reserve4属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESERVE4(String value) {
        this.reserve4 = value;
    }

    /**
     * 获取timestamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIMESTAMP() {
        return timestamp;
    }

    /**
     * 设置timestamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIMESTAMP(String value) {
        this.timestamp = value;
    }

    /**
     * 获取warehousecode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWAREHOUSECODE() {
        return warehousecode;
    }

    /**
     * 设置warehousecode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWAREHOUSECODE(String value) {
        this.warehousecode = value;
    }

}
