
package bcssg.wdssvr.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>FinishWms0Task complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="FinishWms0Task"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entity" type="{http://webservice.wdssvr.bcssg/}wms0TaskEndEntity" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinishWms0Task", propOrder = {
    "entity"
})
public class FinishWms0Task {

    protected Wms0TaskEndEntity entity;

    /**
     * 获取entity属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Wms0TaskEndEntity }
     *     
     */
    public Wms0TaskEndEntity getEntity() {
        return entity;
    }

    /**
     * 设置entity属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Wms0TaskEndEntity }
     *     
     */
    public void setEntity(Wms0TaskEndEntity value) {
        this.entity = value;
    }

}
