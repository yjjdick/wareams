
package bcssg.wdssvr.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>FinishWms1InvTask complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="FinishWms1InvTask"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entity" type="{http://webservice.wdssvr.bcssg/}wms1InvTaskEndEntity" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinishWms1InvTask", propOrder = {
    "entity"
})
public class FinishWms1InvTask {

    protected Wms1InvTaskEndEntity entity;

    /**
     * 获取entity属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Wms1InvTaskEndEntity }
     *     
     */
    public Wms1InvTaskEndEntity getEntity() {
        return entity;
    }

    /**
     * 设置entity属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Wms1InvTaskEndEntity }
     *     
     */
    public void setEntity(Wms1InvTaskEndEntity value) {
        this.entity = value;
    }

}
