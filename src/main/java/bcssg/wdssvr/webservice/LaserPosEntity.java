
package bcssg.wdssvr.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>laserPosEntity complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="laserPosEntity"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CANCELLED" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CAR_ANGLE" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="CAR_HEIGHT" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="EXE_STATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSTRUCTION_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSTRUCTION_SRC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MODEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NODE_X1" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="NODE_X2" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="NODE_Y1" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="NODE_Y2" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="PALLET_LOC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="STATE_INFO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="STOCK_AREA_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TASK_STATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WAREHOUSE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "laserPosEntity", propOrder = {
    "cancelled",
    "carangle",
    "carheight",
    "exestate",
    "instructioncode",
    "instructionsrc",
    "model",
    "nodex1",
    "nodex2",
    "nodey1",
    "nodey2",
    "palletloc",
    "stateinfo",
    "stockareacode",
    "taskstate",
    "type",
    "warehousecode"
})
public class LaserPosEntity {

    @XmlElement(name = "CANCELLED")
    protected int cancelled;
    @XmlElement(name = "CAR_ANGLE")
    protected float carangle;
    @XmlElement(name = "CAR_HEIGHT")
    protected float carheight;
    @XmlElement(name = "EXE_STATE")
    protected String exestate;
    @XmlElement(name = "INSTRUCTION_CODE")
    protected String instructioncode;
    @XmlElement(name = "INSTRUCTION_SRC")
    protected String instructionsrc;
    @XmlElement(name = "MODEL")
    protected String model;
    @XmlElement(name = "NODE_X1")
    protected float nodex1;
    @XmlElement(name = "NODE_X2")
    protected float nodex2;
    @XmlElement(name = "NODE_Y1")
    protected float nodey1;
    @XmlElement(name = "NODE_Y2")
    protected float nodey2;
    @XmlElement(name = "PALLET_LOC")
    protected String palletloc;
    @XmlElement(name = "STATE_INFO")
    protected String stateinfo;
    @XmlElement(name = "STOCK_AREA_CODE")
    protected String stockareacode;
    @XmlElement(name = "TASK_STATE")
    protected String taskstate;
    @XmlElement(name = "TYPE")
    protected String type;
    @XmlElement(name = "WAREHOUSE_CODE")
    protected String warehousecode;

    /**
     * 获取cancelled属性的值。
     * 
     */
    public int getCANCELLED() {
        return cancelled;
    }

    /**
     * 设置cancelled属性的值。
     * 
     */
    public void setCANCELLED(int value) {
        this.cancelled = value;
    }

    /**
     * 获取carangle属性的值。
     * 
     */
    public float getCARANGLE() {
        return carangle;
    }

    /**
     * 设置carangle属性的值。
     * 
     */
    public void setCARANGLE(float value) {
        this.carangle = value;
    }

    /**
     * 获取carheight属性的值。
     * 
     */
    public float getCARHEIGHT() {
        return carheight;
    }

    /**
     * 设置carheight属性的值。
     * 
     */
    public void setCARHEIGHT(float value) {
        this.carheight = value;
    }

    /**
     * 获取exestate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXESTATE() {
        return exestate;
    }

    /**
     * 设置exestate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXESTATE(String value) {
        this.exestate = value;
    }

    /**
     * 获取instructioncode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTRUCTIONCODE() {
        return instructioncode;
    }

    /**
     * 设置instructioncode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTRUCTIONCODE(String value) {
        this.instructioncode = value;
    }

    /**
     * 获取instructionsrc属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTRUCTIONSRC() {
        return instructionsrc;
    }

    /**
     * 设置instructionsrc属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTRUCTIONSRC(String value) {
        this.instructionsrc = value;
    }

    /**
     * 获取model属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODEL() {
        return model;
    }

    /**
     * 设置model属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODEL(String value) {
        this.model = value;
    }

    /**
     * 获取nodex1属性的值。
     * 
     */
    public float getNODEX1() {
        return nodex1;
    }

    /**
     * 设置nodex1属性的值。
     * 
     */
    public void setNODEX1(float value) {
        this.nodex1 = value;
    }

    /**
     * 获取nodex2属性的值。
     * 
     */
    public float getNODEX2() {
        return nodex2;
    }

    /**
     * 设置nodex2属性的值。
     * 
     */
    public void setNODEX2(float value) {
        this.nodex2 = value;
    }

    /**
     * 获取nodey1属性的值。
     * 
     */
    public float getNODEY1() {
        return nodey1;
    }

    /**
     * 设置nodey1属性的值。
     * 
     */
    public void setNODEY1(float value) {
        this.nodey1 = value;
    }

    /**
     * 获取nodey2属性的值。
     * 
     */
    public float getNODEY2() {
        return nodey2;
    }

    /**
     * 设置nodey2属性的值。
     * 
     */
    public void setNODEY2(float value) {
        this.nodey2 = value;
    }

    /**
     * 获取palletloc属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPALLETLOC() {
        return palletloc;
    }

    /**
     * 设置palletloc属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPALLETLOC(String value) {
        this.palletloc = value;
    }

    /**
     * 获取stateinfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTATEINFO() {
        return stateinfo;
    }

    /**
     * 设置stateinfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTATEINFO(String value) {
        this.stateinfo = value;
    }

    /**
     * 获取stockareacode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTOCKAREACODE() {
        return stockareacode;
    }

    /**
     * 设置stockareacode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTOCKAREACODE(String value) {
        this.stockareacode = value;
    }

    /**
     * 获取taskstate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTASKSTATE() {
        return taskstate;
    }

    /**
     * 设置taskstate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTASKSTATE(String value) {
        this.taskstate = value;
    }

    /**
     * 获取type属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTYPE() {
        return type;
    }

    /**
     * 设置type属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTYPE(String value) {
        this.type = value;
    }

    /**
     * 获取warehousecode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWAREHOUSECODE() {
        return warehousecode;
    }

    /**
     * 设置warehousecode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWAREHOUSECODE(String value) {
        this.warehousecode = value;
    }

}
