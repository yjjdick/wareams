
package bcssg.wdssvr.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bcssg.wdssvr.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FinishWms0Task_QNAME = new QName("http://webservice.wdssvr.bcssg/", "FinishWms0Task");
    private final static QName _FinishWms0TaskResponse_QNAME = new QName("http://webservice.wdssvr.bcssg/", "FinishWms0TaskResponse");
    private final static QName _FinishWms1InvTask_QNAME = new QName("http://webservice.wdssvr.bcssg/", "FinishWms1InvTask");
    private final static QName _FinishWms1InvTaskResponse_QNAME = new QName("http://webservice.wdssvr.bcssg/", "FinishWms1InvTaskResponse");
    private final static QName _FinishWms1Task_QNAME = new QName("http://webservice.wdssvr.bcssg/", "FinishWms1Task");
    private final static QName _FinishWms1TaskJson_QNAME = new QName("http://webservice.wdssvr.bcssg/", "FinishWms1TaskJson");
    private final static QName _FinishWms1TaskJsonResponse_QNAME = new QName("http://webservice.wdssvr.bcssg/", "FinishWms1TaskJsonResponse");
    private final static QName _FinishWms1TaskResponse_QNAME = new QName("http://webservice.wdssvr.bcssg/", "FinishWms1TaskResponse");
    private final static QName _RequestPallet_QNAME = new QName("http://webservice.wdssvr.bcssg/", "RequestPallet");
    private final static QName _RequestPalletResponse_QNAME = new QName("http://webservice.wdssvr.bcssg/", "RequestPalletResponse");
    private final static QName _RequestRunning_QNAME = new QName("http://webservice.wdssvr.bcssg/", "RequestRunning");
    private final static QName _RequestRunningResponse_QNAME = new QName("http://webservice.wdssvr.bcssg/", "RequestRunningResponse");
    private final static QName _SendDeviceStatus_QNAME = new QName("http://webservice.wdssvr.bcssg/", "SendDeviceStatus");
    private final static QName _SendDeviceStatusJson_QNAME = new QName("http://webservice.wdssvr.bcssg/", "SendDeviceStatusJson");
    private final static QName _SendDeviceStatusJsonResponse_QNAME = new QName("http://webservice.wdssvr.bcssg/", "SendDeviceStatusJsonResponse");
    private final static QName _SendDeviceStatusResponse_QNAME = new QName("http://webservice.wdssvr.bcssg/", "SendDeviceStatusResponse");
    private final static QName _SendLaserPoss_QNAME = new QName("http://webservice.wdssvr.bcssg/", "SendLaserPoss");
    private final static QName _SendLaserPossResponse_QNAME = new QName("http://webservice.wdssvr.bcssg/", "SendLaserPossResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bcssg.wdssvr.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FinishWms0Task }
     * 
     */
    public FinishWms0Task createFinishWms0Task() {
        return new FinishWms0Task();
    }

    /**
     * Create an instance of {@link FinishWms0TaskResponse }
     * 
     */
    public FinishWms0TaskResponse createFinishWms0TaskResponse() {
        return new FinishWms0TaskResponse();
    }

    /**
     * Create an instance of {@link FinishWms1InvTask }
     * 
     */
    public FinishWms1InvTask createFinishWms1InvTask() {
        return new FinishWms1InvTask();
    }

    /**
     * Create an instance of {@link FinishWms1InvTaskResponse }
     * 
     */
    public FinishWms1InvTaskResponse createFinishWms1InvTaskResponse() {
        return new FinishWms1InvTaskResponse();
    }

    /**
     * Create an instance of {@link FinishWms1Task }
     * 
     */
    public FinishWms1Task createFinishWms1Task() {
        return new FinishWms1Task();
    }

    /**
     * Create an instance of {@link FinishWms1TaskJson }
     * 
     */
    public FinishWms1TaskJson createFinishWms1TaskJson() {
        return new FinishWms1TaskJson();
    }

    /**
     * Create an instance of {@link FinishWms1TaskJsonResponse }
     * 
     */
    public FinishWms1TaskJsonResponse createFinishWms1TaskJsonResponse() {
        return new FinishWms1TaskJsonResponse();
    }

    /**
     * Create an instance of {@link FinishWms1TaskResponse }
     * 
     */
    public FinishWms1TaskResponse createFinishWms1TaskResponse() {
        return new FinishWms1TaskResponse();
    }

    /**
     * Create an instance of {@link RequestPallet }
     * 
     */
    public RequestPallet createRequestPallet() {
        return new RequestPallet();
    }

    /**
     * Create an instance of {@link RequestPalletResponse }
     * 
     */
    public RequestPalletResponse createRequestPalletResponse() {
        return new RequestPalletResponse();
    }

    /**
     * Create an instance of {@link RequestRunning }
     * 
     */
    public RequestRunning createRequestRunning() {
        return new RequestRunning();
    }

    /**
     * Create an instance of {@link RequestRunningResponse }
     * 
     */
    public RequestRunningResponse createRequestRunningResponse() {
        return new RequestRunningResponse();
    }

    /**
     * Create an instance of {@link SendDeviceStatus }
     * 
     */
    public SendDeviceStatus createSendDeviceStatus() {
        return new SendDeviceStatus();
    }

    /**
     * Create an instance of {@link SendDeviceStatusJson }
     * 
     */
    public SendDeviceStatusJson createSendDeviceStatusJson() {
        return new SendDeviceStatusJson();
    }

    /**
     * Create an instance of {@link SendDeviceStatusJsonResponse }
     * 
     */
    public SendDeviceStatusJsonResponse createSendDeviceStatusJsonResponse() {
        return new SendDeviceStatusJsonResponse();
    }

    /**
     * Create an instance of {@link SendDeviceStatusResponse }
     * 
     */
    public SendDeviceStatusResponse createSendDeviceStatusResponse() {
        return new SendDeviceStatusResponse();
    }

    /**
     * Create an instance of {@link SendLaserPoss }
     * 
     */
    public SendLaserPoss createSendLaserPoss() {
        return new SendLaserPoss();
    }

    /**
     * Create an instance of {@link SendLaserPossResponse }
     * 
     */
    public SendLaserPossResponse createSendLaserPossResponse() {
        return new SendLaserPossResponse();
    }

    /**
     * Create an instance of {@link Wms0TaskEndEntity }
     * 
     */
    public Wms0TaskEndEntity createWms0TaskEndEntity() {
        return new Wms0TaskEndEntity();
    }

    /**
     * Create an instance of {@link ResponseResult }
     * 
     */
    public ResponseResult createResponseResult() {
        return new ResponseResult();
    }

    /**
     * Create an instance of {@link Wms1InvTaskEndEntity }
     * 
     */
    public Wms1InvTaskEndEntity createWms1InvTaskEndEntity() {
        return new Wms1InvTaskEndEntity();
    }

    /**
     * Create an instance of {@link Wms1TaskEndEntity }
     * 
     */
    public Wms1TaskEndEntity createWms1TaskEndEntity() {
        return new Wms1TaskEndEntity();
    }

    /**
     * Create an instance of {@link DeviceStatusEntity }
     * 
     */
    public DeviceStatusEntity createDeviceStatusEntity() {
        return new DeviceStatusEntity();
    }

    /**
     * Create an instance of {@link LaserPosEntity }
     * 
     */
    public LaserPosEntity createLaserPosEntity() {
        return new LaserPosEntity();
    }

    /**
     * Create an instance of {@link ReqPalletEntity }
     * 
     */
    public ReqPalletEntity createReqPalletEntity() {
        return new ReqPalletEntity();
    }

    /**
     * Create an instance of {@link ReqRunningEntity }
     * 
     */
    public ReqRunningEntity createReqRunningEntity() {
        return new ReqRunningEntity();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishWms0Task }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "FinishWms0Task")
    public JAXBElement<FinishWms0Task> createFinishWms0Task(FinishWms0Task value) {
        return new JAXBElement<FinishWms0Task>(_FinishWms0Task_QNAME, FinishWms0Task.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishWms0TaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "FinishWms0TaskResponse")
    public JAXBElement<FinishWms0TaskResponse> createFinishWms0TaskResponse(FinishWms0TaskResponse value) {
        return new JAXBElement<FinishWms0TaskResponse>(_FinishWms0TaskResponse_QNAME, FinishWms0TaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishWms1InvTask }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "FinishWms1InvTask")
    public JAXBElement<FinishWms1InvTask> createFinishWms1InvTask(FinishWms1InvTask value) {
        return new JAXBElement<FinishWms1InvTask>(_FinishWms1InvTask_QNAME, FinishWms1InvTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishWms1InvTaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "FinishWms1InvTaskResponse")
    public JAXBElement<FinishWms1InvTaskResponse> createFinishWms1InvTaskResponse(FinishWms1InvTaskResponse value) {
        return new JAXBElement<FinishWms1InvTaskResponse>(_FinishWms1InvTaskResponse_QNAME, FinishWms1InvTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishWms1Task }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "FinishWms1Task")
    public JAXBElement<FinishWms1Task> createFinishWms1Task(FinishWms1Task value) {
        return new JAXBElement<FinishWms1Task>(_FinishWms1Task_QNAME, FinishWms1Task.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishWms1TaskJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "FinishWms1TaskJson")
    public JAXBElement<FinishWms1TaskJson> createFinishWms1TaskJson(FinishWms1TaskJson value) {
        return new JAXBElement<FinishWms1TaskJson>(_FinishWms1TaskJson_QNAME, FinishWms1TaskJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishWms1TaskJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "FinishWms1TaskJsonResponse")
    public JAXBElement<FinishWms1TaskJsonResponse> createFinishWms1TaskJsonResponse(FinishWms1TaskJsonResponse value) {
        return new JAXBElement<FinishWms1TaskJsonResponse>(_FinishWms1TaskJsonResponse_QNAME, FinishWms1TaskJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishWms1TaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "FinishWms1TaskResponse")
    public JAXBElement<FinishWms1TaskResponse> createFinishWms1TaskResponse(FinishWms1TaskResponse value) {
        return new JAXBElement<FinishWms1TaskResponse>(_FinishWms1TaskResponse_QNAME, FinishWms1TaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestPallet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "RequestPallet")
    public JAXBElement<RequestPallet> createRequestPallet(RequestPallet value) {
        return new JAXBElement<RequestPallet>(_RequestPallet_QNAME, RequestPallet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestPalletResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "RequestPalletResponse")
    public JAXBElement<RequestPalletResponse> createRequestPalletResponse(RequestPalletResponse value) {
        return new JAXBElement<RequestPalletResponse>(_RequestPalletResponse_QNAME, RequestPalletResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestRunning }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "RequestRunning")
    public JAXBElement<RequestRunning> createRequestRunning(RequestRunning value) {
        return new JAXBElement<RequestRunning>(_RequestRunning_QNAME, RequestRunning.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestRunningResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "RequestRunningResponse")
    public JAXBElement<RequestRunningResponse> createRequestRunningResponse(RequestRunningResponse value) {
        return new JAXBElement<RequestRunningResponse>(_RequestRunningResponse_QNAME, RequestRunningResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendDeviceStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "SendDeviceStatus")
    public JAXBElement<SendDeviceStatus> createSendDeviceStatus(SendDeviceStatus value) {
        return new JAXBElement<SendDeviceStatus>(_SendDeviceStatus_QNAME, SendDeviceStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendDeviceStatusJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "SendDeviceStatusJson")
    public JAXBElement<SendDeviceStatusJson> createSendDeviceStatusJson(SendDeviceStatusJson value) {
        return new JAXBElement<SendDeviceStatusJson>(_SendDeviceStatusJson_QNAME, SendDeviceStatusJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendDeviceStatusJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "SendDeviceStatusJsonResponse")
    public JAXBElement<SendDeviceStatusJsonResponse> createSendDeviceStatusJsonResponse(SendDeviceStatusJsonResponse value) {
        return new JAXBElement<SendDeviceStatusJsonResponse>(_SendDeviceStatusJsonResponse_QNAME, SendDeviceStatusJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendDeviceStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "SendDeviceStatusResponse")
    public JAXBElement<SendDeviceStatusResponse> createSendDeviceStatusResponse(SendDeviceStatusResponse value) {
        return new JAXBElement<SendDeviceStatusResponse>(_SendDeviceStatusResponse_QNAME, SendDeviceStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendLaserPoss }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "SendLaserPoss")
    public JAXBElement<SendLaserPoss> createSendLaserPoss(SendLaserPoss value) {
        return new JAXBElement<SendLaserPoss>(_SendLaserPoss_QNAME, SendLaserPoss.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendLaserPossResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.wdssvr.bcssg/", name = "SendLaserPossResponse")
    public JAXBElement<SendLaserPossResponse> createSendLaserPossResponse(SendLaserPossResponse value) {
        return new JAXBElement<SendLaserPossResponse>(_SendLaserPossResponse_QNAME, SendLaserPossResponse.class, null, value);
    }

}
