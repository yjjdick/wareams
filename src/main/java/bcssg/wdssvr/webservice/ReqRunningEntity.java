
package bcssg.wdssvr.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>reqRunningEntity complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="reqRunningEntity"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DEVICE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEVICE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RUNNING_DIR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WAREHOUSE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WORK_AREA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reqRunningEntity", propOrder = {
    "devicecode",
    "devicetype",
    "runningdir",
    "warehousecode",
    "workarea"
})
public class ReqRunningEntity {

    @XmlElement(name = "DEVICE_CODE")
    protected String devicecode;
    @XmlElement(name = "DEVICE_TYPE")
    protected String devicetype;
    @XmlElement(name = "RUNNING_DIR")
    protected String runningdir;
    @XmlElement(name = "WAREHOUSE_CODE")
    protected String warehousecode;
    @XmlElement(name = "WORK_AREA")
    protected String workarea;

    /**
     * 获取devicecode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEVICECODE() {
        return devicecode;
    }

    /**
     * 设置devicecode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEVICECODE(String value) {
        this.devicecode = value;
    }

    /**
     * 获取devicetype属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEVICETYPE() {
        return devicetype;
    }

    /**
     * 设置devicetype属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEVICETYPE(String value) {
        this.devicetype = value;
    }

    /**
     * 获取runningdir属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRUNNINGDIR() {
        return runningdir;
    }

    /**
     * 设置runningdir属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRUNNINGDIR(String value) {
        this.runningdir = value;
    }

    /**
     * 获取warehousecode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWAREHOUSECODE() {
        return warehousecode;
    }

    /**
     * 设置warehousecode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWAREHOUSECODE(String value) {
        this.warehousecode = value;
    }

    /**
     * 获取workarea属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWORKAREA() {
        return workarea;
    }

    /**
     * 设置workarea属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWORKAREA(String value) {
        this.workarea = value;
    }

}
