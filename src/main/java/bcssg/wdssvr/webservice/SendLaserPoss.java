
package bcssg.wdssvr.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>SendLaserPoss complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="SendLaserPoss"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="listLaserPos" type="{http://webservice.wdssvr.bcssg/}laserPosEntity" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendLaserPoss", propOrder = {
    "listLaserPos"
})
public class SendLaserPoss {

    protected List<LaserPosEntity> listLaserPos;

    /**
     * Gets the value of the listLaserPos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listLaserPos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListLaserPos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LaserPosEntity }
     * 
     * 
     */
    public List<LaserPosEntity> getListLaserPos() {
        if (listLaserPos == null) {
            listLaserPos = new ArrayList<LaserPosEntity>();
        }
        return this.listLaserPos;
    }

}
