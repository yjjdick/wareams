
package bcssg.wdssvr.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>wms1InvTaskEndEntity complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="wms1InvTaskEndEntity"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="INSTRUCTION_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSTRUCTION_SRC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LAYER" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RESERVE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RESERVE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RESERVE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RESERVE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RFID_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="STOCK_AREA_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TIMESTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WAREHOUSE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wms1InvTaskEndEntity", propOrder = {
    "instructioncode",
    "instructionsrc",
    "layer",
    "reserve1",
    "reserve2",
    "reserve3",
    "reserve4",
    "rfidcode",
    "stockareacode",
    "timestamp",
    "warehousecode"
})
public class Wms1InvTaskEndEntity {

    @XmlElement(name = "INSTRUCTION_CODE")
    protected String instructioncode;
    @XmlElement(name = "INSTRUCTION_SRC")
    protected String instructionsrc;
    @XmlElement(name = "LAYER")
    protected int layer;
    @XmlElement(name = "RESERVE1")
    protected String reserve1;
    @XmlElement(name = "RESERVE2")
    protected String reserve2;
    @XmlElement(name = "RESERVE3")
    protected String reserve3;
    @XmlElement(name = "RESERVE4")
    protected String reserve4;
    @XmlElement(name = "RFID_CODE")
    protected String rfidcode;
    @XmlElement(name = "STOCK_AREA_CODE")
    protected String stockareacode;
    @XmlElement(name = "TIMESTAMP")
    protected String timestamp;
    @XmlElement(name = "WAREHOUSE_CODE")
    protected String warehousecode;

    /**
     * 获取instructioncode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTRUCTIONCODE() {
        return instructioncode;
    }

    /**
     * 设置instructioncode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTRUCTIONCODE(String value) {
        this.instructioncode = value;
    }

    /**
     * 获取instructionsrc属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTRUCTIONSRC() {
        return instructionsrc;
    }

    /**
     * 设置instructionsrc属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTRUCTIONSRC(String value) {
        this.instructionsrc = value;
    }

    /**
     * 获取layer属性的值。
     * 
     */
    public int getLAYER() {
        return layer;
    }

    /**
     * 设置layer属性的值。
     * 
     */
    public void setLAYER(int value) {
        this.layer = value;
    }

    /**
     * 获取reserve1属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESERVE1() {
        return reserve1;
    }

    /**
     * 设置reserve1属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESERVE1(String value) {
        this.reserve1 = value;
    }

    /**
     * 获取reserve2属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESERVE2() {
        return reserve2;
    }

    /**
     * 设置reserve2属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESERVE2(String value) {
        this.reserve2 = value;
    }

    /**
     * 获取reserve3属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESERVE3() {
        return reserve3;
    }

    /**
     * 设置reserve3属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESERVE3(String value) {
        this.reserve3 = value;
    }

    /**
     * 获取reserve4属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESERVE4() {
        return reserve4;
    }

    /**
     * 设置reserve4属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESERVE4(String value) {
        this.reserve4 = value;
    }

    /**
     * 获取rfidcode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFIDCODE() {
        return rfidcode;
    }

    /**
     * 设置rfidcode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFIDCODE(String value) {
        this.rfidcode = value;
    }

    /**
     * 获取stockareacode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTOCKAREACODE() {
        return stockareacode;
    }

    /**
     * 设置stockareacode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTOCKAREACODE(String value) {
        this.stockareacode = value;
    }

    /**
     * 获取timestamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIMESTAMP() {
        return timestamp;
    }

    /**
     * 设置timestamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIMESTAMP(String value) {
        this.timestamp = value;
    }

    /**
     * 获取warehousecode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWAREHOUSECODE() {
        return warehousecode;
    }

    /**
     * 设置warehousecode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWAREHOUSECODE(String value) {
        this.warehousecode = value;
    }

}
