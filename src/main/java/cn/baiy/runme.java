package cn.baiy;

public class runme {

	static {
		try 
		{
			System.loadLibrary("bycfg");
		}
		catch (UnsatisfiedLinkError e)
		{
			System.err.println("Native code library failed to load.\n" + e);
			System.exit(1);
		}
	}

	public static void main(String argv[])
	{
		byConfig cfgTest = new byConfig();

		cfgTest.WriteString("key", "stName", "stValue");

		byte[] gbVal = new byte[4];
		gbVal[0] = 0;
		gbVal[1] = 1;
		gbVal[2] = 2;
		gbVal[3] = 3;
		cfgTest.WriteBlob("key", "btName", gbVal);

		cfgTest.WriteDWord("foo", "nName", 999);

		java.math.BigInteger nnVal = new java.math.BigInteger("12345678901234567890");
		cfgTest.WriteQWord("bar", "nnName", nnVal);

		byte[] gbResult = cfgTest.SaveIni();
		System.out.println(new String(gbResult));

		System.out.println("===================================================");
		System.out.println(cfgTest.ReadString("key", "stName"));

		System.out.println("===================================================");
		byVSTR vKeys = cfgTest.GetAllSubkeys();
		for (int i=0; i<vKeys.size(); ++i)
		{
			System.out.println(vKeys.get(i));
		}

		System.out.println("===================================================");
		byVSTRPAIR vVals = cfgTest.GetAllValues(true);
		for (int i=0; i<vVals.size(); ++i)
		{
			bySTRPAIR pstVal = vVals.get(i);
			System.out.println(pstVal.getFirst() + "\t\t" + pstVal.getSecond());
		}
	}
}
