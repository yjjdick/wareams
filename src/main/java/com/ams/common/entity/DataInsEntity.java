package com.ams.common.entity;

import lombok.Data;

import java.util.List;

@Data
public class DataInsEntity {
    List<InsEntity> data;
}
