package com.ams.common.entity;

import lombok.Data;

@Data
public class InsDetailEntity {
    // 物资编码
    String matno;
    // 物资描述
    String matText;
    // 数量
    Float quantity;
    // 批次
    String batch;
    // 单位
    String unit;
}
