package com.ams.common.entity;

import lombok.Data;

import java.util.List;

@Data
public class InsEntity {
    // 仓库编号 JAL2
    String whno;
    // 指令来源1:继远2:机具
    String taskSource;
    // 指令编号 操作指令编号（唯一）
    String taskno;
    // RFID编号
    String rfidCode;
    // 库区编码 A02
    String stoArea;
    // 站点
    String palletLoc;
    // 批次
    String batch;
    // 单位
    String unit;
    // 物资数量
    Float quantity;
    // 操作类型 0：入库1：出库
    String operateType;
    // 容器起始地址
    String binnoFrom;
    // 容器目的地址
    String binnoTo;
    // 指令优先级
    Integer taskLevel;
    // 发送时间
    String taskTime;
    // 物料信息
    List<InsDetailEntity> reserve;
    // 信息
    String message;
    // 状态
    String status;
    String success;
}
