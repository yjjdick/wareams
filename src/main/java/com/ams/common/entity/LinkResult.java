package com.ams.common.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

@Data
public class LinkResult {
    @JSONField(name = "key")
    String key;
    @JSONField(name = "SynchronizedWait")
    Integer synchronizedWait;
    @JSONField(name = "PrefixId")
    Integer prefixId;
    @JSONField(name = "PrefixPort")
    Integer prefixPort;
    @JSONField(name = "Argu")
    List<Integer> argu;
    @JSONField(name = "IfSt")
    String ifSt;
    @JSONField(name = "IfMsg")
    String ifMsg;
    @JSONField(name = "Op")
    Integer op;
    @JSONField(name = "Result")
    List<Integer> result;
}
