package com.ams.common.entity;

import lombok.Data;

@Data
public class OrderTaskResult {
    String whno;
    String taskSource;
    String taskno;
    String status;
    String message;

    public static OrderTaskResult success(String taskno, String taskSource, String whno) {
        OrderTaskResult orderTaskResult = new OrderTaskResult();
        orderTaskResult.setStatus("0");
        orderTaskResult.setMessage("success");
        orderTaskResult.setTaskno(taskno);
        orderTaskResult.setTaskSource(taskSource);
        orderTaskResult.setWhno(whno);
        return orderTaskResult;
    }

    public static OrderTaskResult error(String status, String message, String taskno, String taskSource, String whno) {
        OrderTaskResult orderTaskResult = new OrderTaskResult();
        orderTaskResult.setStatus(status);
        orderTaskResult.setMessage(message);
        orderTaskResult.setTaskno(taskno);
        orderTaskResult.setTaskSource(taskSource);
        orderTaskResult.setWhno(whno);
        return orderTaskResult;
    }
}
