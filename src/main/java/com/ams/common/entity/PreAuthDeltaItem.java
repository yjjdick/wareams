package com.ams.common.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class PreAuthDeltaItem {
    @JSONField(name = "Number")
    String number;
    @JSONField(name = "From")
    Integer from;
    @JSONField(name = "To")
    Integer to;
}
