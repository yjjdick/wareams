package com.ams.common.entity;

import lombok.Data;

@Data
public class RackMisItem {
    String rackId;
    String locationId;
    Integer status;
    Integer expectValue;
}
