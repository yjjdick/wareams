package com.ams.common.entity;

import lombok.Data;

@Data
public class RackQtyItem {
    String rackId;
    String locationId;
    Integer status;
    Integer qty;
}
