package com.ams.common.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StorageLocation {
    String locationId;
    Double status;
    String matId;
    String matName;
    Double rawAdc;
    Double weight;
    Double justness;
    Double lights;
    Integer expected;
    BigDecimal quantity;
}
