package com.ams.common.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StorageMatDiffItem {
    String matId;
    String matName;
    BigDecimal lotQty;
    BigDecimal dbQty;
}
