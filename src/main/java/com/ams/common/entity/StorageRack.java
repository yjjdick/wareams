package com.ams.common.entity;

import lombok.Data;

import java.util.List;

@Data
public class StorageRack {
    String rackId;
    List<StorageLocation> storageLocationList;
}
