package com.ams.common.entity;

import lombok.Data;

@Data
public class WDSResult {
    private String message;
    private String status;
    private String whno;
    private String taskSource;
    private String taskno;

    public WDSResult() {
    }

    public WDSResult(String message, String status, String whno, String taskSource, String taskno) {
        this.message = message;
        this.status = status;
        this.whno = whno;
        this.taskSource = taskSource;
        this.taskno = taskno;
    }
}
