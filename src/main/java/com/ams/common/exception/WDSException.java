package com.ams.common.exception;

import com.ams.enums.ResultEnum;
import lombok.Data;

/**
 * 自定义异常
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年10月27日 下午10:11:27
 */
@Data
public class WDSException extends RuntimeException {
	private static final long serialVersionUID = 1L;

    private String message;
    private String status;
    private String whno;
    private String taskSource;
    private String taskno;

    public WDSException(String message, String status, String whno, String taskSource, String taskno) {
        this.message = message;
        this.status = status;
        this.whno = whno;
        this.taskSource = taskSource;
        this.taskno = taskno;
    }
}
