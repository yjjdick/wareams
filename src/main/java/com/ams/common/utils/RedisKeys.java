package com.ams.common.utils;

/**
 * Redis所有Keys
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-07-18 19:51
 */
public class RedisKeys {

    public final static String RACK_MIS_LIST = "rack_mis_list"; // hset

    public final static String LINK_TRAY_ID = "link_tray_id";

    public final static String PRE_QTY_MSG = "pre_qty_msg"; // hset

    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }
}
