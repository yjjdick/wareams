package com.ams.common.utils;

/**
 * 系统参数相关Key
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-03-26 10:33
 */
public class SnConstant {

    public final static String DOC_ASN_PREFIX = "asn";
    public final static String DOC_ASN_DETAIL_PREFIX = "ad";
    public final static String DOC_ORDER_PREFIX = "order";
    public final static String DOC_ORDER_DETAIL_PREFIX = "od";
}
