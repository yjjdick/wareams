package com.ams.config;

import com.ams.webservice.IWcs2wdsService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

@Configuration
public class WebServiceConfig {

    @Autowired
    private Bus bus;

    @Autowired
    IWcs2wdsService wcs2wdsService;

    @Bean
    public Endpoint endpointUserService() {
        EndpointImpl endpoint = new EndpointImpl(bus, wcs2wdsService);
        endpoint.publish("/Wcs2wdsService");//接口发布在 /Wcs2wdsService 目录下
        return endpoint;
    }

}
