package com.ams.controller;

import com.ams.common.utils.PageUtils;
import com.ams.common.utils.R;
import com.ams.service.BasLocationService;
import com.ams.model.BasLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/sys/basLocation")
public class SysBasLocationController {

	@Autowired
	private BasLocationService basLocationService;

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = basLocationService.queryPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 详情
	 */
	@ResponseBody
	@GetMapping("/info/{id}")
	public R info(@PathVariable Object id){
        BasLocation basLocation = basLocationService.findById(id);
		return R.ok().put("basLocation", basLocation);
	}

	/**
	 * 更新
	 */
	@ResponseBody
	@PostMapping("/update")
	public R update(@RequestBody BasLocation basLocation) {
		basLocation.update();
		return R.ok();
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@PostMapping("/save")
	public R save(@RequestBody BasLocation basLocation) {
		basLocation.save();
		return R.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public R delete(@RequestBody Object[] ids) {
        basLocationService.deleteBatch(ids);
        return R.ok();
    }

}