package com.ams.controller;

import com.ams.common.utils.PageUtils;
import com.ams.common.utils.R;
import com.ams.service.DocAsnDetailsService;
import com.ams.model.DocAsnDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/sys/docAsnDetails")
public class SysDocAsnDetailsController {

	@Autowired
	private DocAsnDetailsService docAsnDetailsService;

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = docAsnDetailsService.queryPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 详情
	 */
	@ResponseBody
	@GetMapping("/info/{id}")
	public R info(@PathVariable Object id){
        DocAsnDetails docAsnDetails = docAsnDetailsService.findById(id);
		return R.ok().put("docAsnDetails", docAsnDetails);
	}

	/**
	 * 更新
	 */
	@ResponseBody
	@PostMapping("/update")
	public R update(@RequestBody DocAsnDetails docAsnDetails) {
		docAsnDetails.update();
		return R.ok();
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@PostMapping("/save")
	public R save(@RequestBody DocAsnDetails docAsnDetails) {
		docAsnDetails.save();
		return R.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public R delete(@RequestBody Object[] ids) {
        docAsnDetailsService.deleteBatch(ids);
        return R.ok();
    }

}