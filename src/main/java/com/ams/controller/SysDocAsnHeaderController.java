package com.ams.controller;

import com.ams.common.utils.PageUtils;
import com.ams.common.utils.R;
import com.ams.service.DocAsnHeaderService;
import com.ams.model.DocAsnHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/sys/docAsnHeader")
public class SysDocAsnHeaderController {

	@Autowired
	private DocAsnHeaderService docAsnHeaderService;

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = docAsnHeaderService.queryPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 详情
	 */
	@ResponseBody
	@GetMapping("/info/{id}")
	public R info(@PathVariable Object id){
        DocAsnHeader docAsnHeader = docAsnHeaderService.findById(id);
		return R.ok().put("docAsnHeader", docAsnHeader);
	}

	/**
	 * 更新
	 */
	@ResponseBody
	@PostMapping("/update")
	public R update(@RequestBody DocAsnHeader docAsnHeader) {
		docAsnHeader.update();
		return R.ok();
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@PostMapping("/save")
	public R save(@RequestBody DocAsnHeader docAsnHeader) {
		docAsnHeader.save();
		return R.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public R delete(@RequestBody Object[] ids) {
        docAsnHeaderService.deleteBatch(ids);
        return R.ok();
    }

}