package com.ams.controller;

import com.ams.common.utils.PageUtils;
import com.ams.common.utils.R;
import com.ams.service.DocOrderDetailsService;
import com.ams.model.DocOrderDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/sys/docOrderDetails")
public class SysDocOrderDetailsController {

	@Autowired
	private DocOrderDetailsService docOrderDetailsService;

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = docOrderDetailsService.queryPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 详情
	 */
	@ResponseBody
	@GetMapping("/info/{id}")
	public R info(@PathVariable Object id){
        DocOrderDetails docOrderDetails = docOrderDetailsService.findById(id);
		return R.ok().put("docOrderDetails", docOrderDetails);
	}

	/**
	 * 更新
	 */
	@ResponseBody
	@PostMapping("/update")
	public R update(@RequestBody DocOrderDetails docOrderDetails) {
		docOrderDetails.update();
		return R.ok();
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@PostMapping("/save")
	public R save(@RequestBody DocOrderDetails docOrderDetails) {
		docOrderDetails.save();
		return R.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public R delete(@RequestBody Object[] ids) {
        docOrderDetailsService.deleteBatch(ids);
        return R.ok();
    }

}