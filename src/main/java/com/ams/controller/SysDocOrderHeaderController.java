package com.ams.controller;

import com.ams.common.utils.PageUtils;
import com.ams.common.utils.R;
import com.ams.service.DocOrderHeaderService;
import com.ams.model.DocOrderHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/sys/docOrderHeader")
public class SysDocOrderHeaderController {

	@Autowired
	private DocOrderHeaderService docOrderHeaderService;

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = docOrderHeaderService.queryPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 详情
	 */
	@ResponseBody
	@GetMapping("/info/{id}")
	public R info(@PathVariable Object id){
        DocOrderHeader docOrderHeader = docOrderHeaderService.findById(id);
		return R.ok().put("docOrderHeader", docOrderHeader);
	}

	/**
	 * 更新
	 */
	@ResponseBody
	@PostMapping("/update")
	public R update(@RequestBody DocOrderHeader docOrderHeader) {
		docOrderHeader.update();
		return R.ok();
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@PostMapping("/save")
	public R save(@RequestBody DocOrderHeader docOrderHeader) {
		docOrderHeader.save();
		return R.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public R delete(@RequestBody Object[] ids) {
        docOrderHeaderService.deleteBatch(ids);
        return R.ok();
    }

}