package com.ams.controller;

import com.ams.common.utils.PageUtils;
import com.ams.common.utils.R;
import com.ams.service.EntrancePrivilegeService;
import com.ams.model.EntrancePrivilege;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/sys/entrancePrivilege")
public class SysEntrancePrivilegeController {

	@Autowired
	private EntrancePrivilegeService entrancePrivilegeService;

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = entrancePrivilegeService.queryPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 详情
	 */
	@ResponseBody
	@GetMapping("/info/{id}")
	public R info(@PathVariable Object id){
        EntrancePrivilege entrancePrivilege = entrancePrivilegeService.findById(id);
		return R.ok().put("entrancePrivilege", entrancePrivilege);
	}

	/**
	 * 更新
	 */
	@ResponseBody
	@PostMapping("/update")
	public R update(@RequestBody EntrancePrivilege entrancePrivilege) {
		entrancePrivilege.update();
		return R.ok();
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@PostMapping("/save")
	public R save(@RequestBody EntrancePrivilege entrancePrivilege) {
		entrancePrivilege.save();
		return R.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public R delete(@RequestBody Object[] ids) {
        entrancePrivilegeService.deleteBatch(ids);
        return R.ok();
    }

}