package com.ams.controller;

import com.ams.common.utils.PageUtils;
import com.ams.common.utils.R;
import com.ams.service.InvLotLocIdService;
import com.ams.model.InvLotLocId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/sys/invLotLocId")
public class SysInvLotLocIdController {

	@Autowired
	private InvLotLocIdService invLotLocIdService;

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = invLotLocIdService.queryPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 详情
	 */
	@ResponseBody
	@GetMapping("/info/{id}")
	public R info(@PathVariable Object id){
        InvLotLocId invLotLocId = invLotLocIdService.findById(id);
		return R.ok().put("invLotLocId", invLotLocId);
	}

	/**
	 * 更新
	 */
	@ResponseBody
	@PostMapping("/update")
	public R update(@RequestBody InvLotLocId invLotLocId) {
		invLotLocId.update();
		return R.ok();
	}

	/**
	 * 新增
	 */
	@ResponseBody
	@PostMapping("/save")
	public R save(@RequestBody InvLotLocId invLotLocId) {
		invLotLocId.save();
		return R.ok();
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public R delete(@RequestBody Object[] ids) {
        invLotLocIdService.deleteBatch(ids);
        return R.ok();
    }

}