package com.ams.controller;

import com.jfinal.kit.PathKit;
import com.lly835.bestpay.model.PayResponse;
import com.ams.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Controller
@RequestMapping("/test")
@Slf4j
public class TestController {

    @GetMapping("/hello")
    public ModelAndView hello(ModelAndView modelAndView, HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-disposition", "attachment; filename=test.doc");
        response.setContentType("application/octet-stream;charset=UTF-8");
        modelAndView.addObject("value", "test");
        modelAndView.setViewName("hello");
        return modelAndView;
    }

    @GetMapping("/ip")
    @ResponseBody
    public String ip() {
        String host = null;
        try {
            host = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.error("get server host Exception e:", e);
        }
        return host;
    }

    public static void main(String[] args) {
        String baseTemplatePath = new StringBuilder(PathKit.getRootClassPath())
                .append("/")
//                .append(PathKit.getPackagePath(this))
//                .append("/tpl")
                .toString();
        log.info(System.getProperty("user.dir"));
    }
}