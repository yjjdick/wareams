package com.ams.dao;

import com.ams.dao.BaseDao;
import com.ams.model.BasLocation;
import org.springframework.stereotype.Component;

@Component
public class BasLocationDao extends BaseDao<BasLocation> {
    public BasLocationDao() {
        super(BasLocation.class);
    }
}