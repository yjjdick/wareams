package com.ams.dao;

import com.ams.dao.BaseDao;
import com.ams.model.BasSku;
import org.springframework.stereotype.Component;

@Component
public class BasSkuDao extends BaseDao<BasSku> {
    public BasSkuDao() {
        super(BasSku.class);
    }
}