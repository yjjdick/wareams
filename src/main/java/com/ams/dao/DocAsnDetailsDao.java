package com.ams.dao;

import com.ams.dao.BaseDao;
import com.ams.model.DocAsnDetails;
import org.springframework.stereotype.Component;

@Component
public class DocAsnDetailsDao extends BaseDao<DocAsnDetails> {
    public DocAsnDetailsDao() {
        super(DocAsnDetails.class);
    }
}