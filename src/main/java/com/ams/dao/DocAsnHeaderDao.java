package com.ams.dao;

import com.ams.dao.BaseDao;
import com.ams.model.DocAsnHeader;
import org.springframework.stereotype.Component;

@Component
public class DocAsnHeaderDao extends BaseDao<DocAsnHeader> {
    public DocAsnHeaderDao() {
        super(DocAsnHeader.class);
    }
}