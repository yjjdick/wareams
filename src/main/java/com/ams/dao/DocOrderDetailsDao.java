package com.ams.dao;

import com.ams.dao.BaseDao;
import com.ams.model.DocOrderDetails;
import org.springframework.stereotype.Component;

@Component
public class DocOrderDetailsDao extends BaseDao<DocOrderDetails> {
    public DocOrderDetailsDao() {
        super(DocOrderDetails.class);
    }
}