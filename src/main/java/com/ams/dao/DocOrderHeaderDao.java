package com.ams.dao;

import com.ams.dao.BaseDao;
import com.ams.model.DocOrderHeader;
import org.springframework.stereotype.Component;

@Component
public class DocOrderHeaderDao extends BaseDao<DocOrderHeader> {
    public DocOrderHeaderDao() {
        super(DocOrderHeader.class);
    }
}