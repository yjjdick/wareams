package com.ams.dao;

import com.ams.dao.BaseDao;
import com.ams.model.EntrancePrivilege;
import org.springframework.stereotype.Component;

@Component
public class EntrancePrivilegeDao extends BaseDao<EntrancePrivilege> {
    public EntrancePrivilegeDao() {
        super(EntrancePrivilege.class);
    }
}