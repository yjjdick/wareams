package com.ams.dao;

import com.ams.dao.BaseDao;
import com.ams.model.InvLotLocId;
import org.springframework.stereotype.Component;

@Component
public class InvLotLocIdDao extends BaseDao<InvLotLocId> {
    public InvLotLocIdDao() {
        super(InvLotLocId.class);
    }
}