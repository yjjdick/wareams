package com.ams.dao;

import com.ams.model.ScheduleJobLog;
import org.springframework.stereotype.Component;

@Component
public class ScheduleJobLogDao extends BaseDao<ScheduleJobLog> {
    public ScheduleJobLogDao() {
        super(ScheduleJobLog.class);
    }
}
