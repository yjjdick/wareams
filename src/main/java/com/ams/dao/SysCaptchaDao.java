package com.ams.dao;

import com.ams.model.SysCaptcha;
import org.springframework.stereotype.Component;

@Component
public class SysCaptchaDao extends BaseDao<SysCaptcha> {
    public SysCaptchaDao() {
        super(SysCaptcha.class);
    }
}
