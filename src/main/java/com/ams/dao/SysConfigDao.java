package com.ams.dao;

import com.ams.model.SysConfig;
import org.springframework.stereotype.Component;

@Component
public class SysConfigDao extends BaseDao<SysConfig> {
    public SysConfigDao() {
        super(SysConfig.class);
    }
}
