package com.ams.dao;

import com.ams.model.SysLog;
import org.springframework.stereotype.Component;

@Component
public class SysLogDao extends BaseDao<SysLog> {
    public SysLogDao() {
        super(SysLog.class);
    }
}
