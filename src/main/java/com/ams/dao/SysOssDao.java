package com.ams.dao;

import com.ams.model.SysOss;
import org.springframework.stereotype.Component;

@Component
public class SysOssDao extends BaseDao<SysOss> {
    public SysOssDao() {
        super(SysOss.class);
    }
}
