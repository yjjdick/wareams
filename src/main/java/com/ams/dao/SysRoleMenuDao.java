package com.ams.dao;

import com.ams.model.SysRoleMenu;
import org.springframework.stereotype.Component;

@Component
public class SysRoleMenuDao extends BaseDao<SysRoleMenu> {
    public SysRoleMenuDao() {
        super(SysRoleMenu.class);
    }
}
