package com.ams.enums;

import lombok.Getter;

@Getter
public enum EntranceStatus {
    ACCESS(1,"00"),
    ENTER(2,"10"),
    FINISH(3,"99"),
    CANCEL(4,"90"),
    EXIT(5,"80")
    ;

    private Integer code;
    private String message;
    EntranceStatus(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
