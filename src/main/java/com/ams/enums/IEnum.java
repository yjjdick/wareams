package com.ams.enums;

public interface IEnum {
    Integer getCode();
    String getMessage();
}
