package com.ams.enums;

import lombok.Getter;

@Getter
public enum LightOperationStatus {
    RESET(-1,"ResetLight"), // 复位
    NORMAL(0,"YellowLightUp"), // 待操作提示——黄灯常亮：入库时物料还未全部放完（入库未放或少放）；出库时物料还未全部取完（出库未拿或少拿）。
    PART_FINISH(1,"GreenLightUpAndAlert"),// 操作正确提示——绿灯常亮+蜂鸣1-2秒：某物料全部操作正常结束，且无所退换盘，结束后转黄灯常亮。
    CHANGE(2,"RedYellowGreenLightUpAndAlert"),// 退换盘提示——红黄绿同时常亮+蜂鸣：某物料全部（或部分）操作正常结束且需要换盘或退盘。换盘结束亮黄灯常亮；退盘结束亮绿灯闪现。
    ALL_FINISH(3,"GreenLightBlink"),// 操作结束提示——绿灯闪现：整单操作完毕。
    MIS(4,"RedLightUpAndAlert"),// 操作异常提示——红灯常亮+蜂鸣：入库时，放入错误物料或者放入错误货位；出库时，取出错误物料。直到操作修正，转为黄灯常亮。无单据操作时：
    EQUIPMENT_MIS(5,"RedLightBlink"),// 设备异常——红灯闪现
    EQUIPMENT_NOR(6,"GreenLightBlink"),// 设备正常——绿灯闪现
    EQUIPMENT_WARN(7,"YellowLightBlink"),// 设备警告——黄灯闪现
    ;

    private Integer code;
    private String message;
    LightOperationStatus(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
