package com.ams.enums;

import lombok.Getter;

@Getter
public enum LinkOperationStatus {
    STATUS(1,"status"),
    CALL(2,"call"),
    RETURN(3,"return"),
    ;

    private Integer code;
    private String message;
    LinkOperationStatus(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
