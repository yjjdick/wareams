package com.ams.enums;

import lombok.Getter;

@Getter
public enum LinkResultStatus {
    TRAY_DISABLE(1,"tray_disable"), // 托盘不可用
    LOCATION_DISABLED(2,"location_disabled"), // 位置不可用
    LOCATION_TRAY_EXSITS(3,"location_tray_exsits"), // 该传输位置有托盘
    TRAY_CALLED(4,"tray_called"), // 托盘已调用
    TRANS_LOC_DISABLE(5,"trans_loc_disable"), // 传输位置不可用或用户未在操作屏登录
    ;

    private Integer code;
    private String message;
    LinkResultStatus(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
