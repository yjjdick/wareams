package com.ams.enums;

import lombok.Getter;

@Getter
public enum OrderDetailStatusEnum {
    // 00创建 10操作中 99完成 90取消
    CREATE(1,"00"),
    OPERATION(2,"10"),
    FINISHED(3,"99"),
    CANCEL(0,"90"),
    ;

    private Integer code;
    private String message;
    OrderDetailStatusEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
