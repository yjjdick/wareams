package com.ams.enums;

import lombok.Getter;

@Getter
public enum OrderTaskTypeEnum {
    STOCK_OUT(0,"0"),// 出库
    STOCK_IN(1,"1"), // 入库
    ;

    private Integer code;
    private String message;
    OrderTaskTypeEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
