package com.ams.enums;

import lombok.Getter;

@Getter
public enum ResultEnum {
    SYSTEM_SUCCESS(0,"成功"),//成功
    SYSTM_ERROR(500, "系统错误"),//系统错误
    PARAM_ERROR(1, "参数不正确"),//参数不正确
    OPERATE_NOT_EXISTS(1001, "操作类型不存在"),//操作类型不存在
    MAT_DETAIL_REPETITION(1002, "物料明细中有重复编码"),//物料明细中有重复编码
    UPLOAD_FILE_BIG(1003, "上传文件太大"),//上传文件太大
    STOCK_OUT_ENOUGH(1004, "库存不足"),
    WS_TASKNO_NOT_FOUND(1005, "没有找到指令编号"),
    API_ENTRANCEID_NOT_EXSITS(1006, "门禁ID不存在"),
    API_ENTRANCE_STATUS_ERROR(1007, "门禁状态有误"),
    API_ENTRANCE_ORDERNO_NOT_EXISTS(1008, "门禁订单号没找到"),
    API_ENTRANCE_USER_ACCESS_EXISTS(1009, "门禁中已有准入人员同时只能有一个"),
    ;

    private Integer code;
    private String message;
    ResultEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
