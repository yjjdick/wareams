package com.ams.enums;

import lombok.Getter;

@Getter
public enum SnEnum {
    DOC_ASN(1,"docasn"),//入库sn
    DOC_ORDER(2,"docorder"),//出库sn
    DOC_ASN_DETAIL(3,"docorder_detail"),//出库sn detail
    DOC_ORDER_DETAIL(4,"docorder_detail"),//出库sn detail
    ;

    private Integer code;
    private String message;
    SnEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
