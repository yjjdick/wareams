package com.ams.handler.baiy;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.util.List;

public interface IBaiyHandler {
    boolean handle(List<Object> dataArr);
}
