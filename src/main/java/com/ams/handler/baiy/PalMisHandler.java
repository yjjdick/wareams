package com.ams.handler.baiy;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ams.common.entity.RackMisItem;
import com.ams.common.utils.RedisKeys;
import com.ams.common.utils.RedisUtils;
import com.ams.common.utils.SnConstant;
import com.ams.enums.EntranceStatus;
import com.ams.enums.LightOperationStatus;
import com.ams.model.BasLocation;
import com.ams.model.DocAsnDetails;
import com.ams.model.DocOrderDetails;
import com.ams.model.EntrancePrivilege;
import com.ams.service.*;
import com.ams.service.impl.WcsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class PalMisHandler implements IBaiyHandler {

    @Autowired
    EntrancePrivilegeService entrancePrivilegeService;

    @Autowired
    DocAsnDetailsService docAsnDetailsService;

    @Autowired
    DocOrderDetailsService docOrderDetailsService;

    @Autowired
    BasLocationService basLocationService;

    @Autowired
    InvLotLocIdService invLotLocIdService;

    @Autowired
    WcsService wcsService;

    @Autowired
    RedisUtils redisUtils;

    @Override
    public boolean handle(List<Object> dataArr) {
//        log.info("【PalMisHandler handle】start >>>>>>>>>>>>>>>>");
//        EntrancePrivilege entrancePrivilegeQuery = new EntrancePrivilege();
//        entrancePrivilegeQuery.setEntrancestatus(EntranceStatus.ENTER.getMessage());
//        EntrancePrivilege entrancePrivilege = entrancePrivilegeService.findFirstByModel(entrancePrivilegeQuery);
//        if (entrancePrivilege == null) {
//            return false;
//        }
//        log.info("【PalMisHandler handle】EntrancePrivilege enter >>>>>>>>>>>>>>>>");
//        String orderno = entrancePrivilege.getOrderno();
        String rackId = (String)dataArr.get(1);
        String locationId = (String)dataArr.get(2);
        Integer status = (Integer)dataArr.get(3);
        Integer expectValue = (Integer)dataArr.get(4);
        log.info("【PalQtyHandler handle】rackId = {} locationId = {} status = {} expectValue ={}>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", rackId, locationId, status, expectValue);
        //异物入侵解除
        if(status == 0x0004) {
            log.info("【PalQtyHandler handle】状态异常解除>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            String data = redisUtils.hget(RedisKeys.RACK_MIS_LIST, rackId + ":" + locationId);
            if(data != null) {
                redisUtils.hdel(RedisKeys.RACK_MIS_LIST, rackId + ":" + locationId);
                if (redisUtils.hsize(RedisKeys.RACK_MIS_LIST) == 0) {
                    // 复位灯
                    try {
                        wcsService.lightOperation(LightOperationStatus.NORMAL);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else {
                // 复位灯
                try {
                    wcsService.lightOperation(LightOperationStatus.NORMAL);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            log.info("【PalQtyHandler handle】其他错误>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            RackMisItem rackMisItem = new RackMisItem();
            rackMisItem.setLocationId(locationId);
            rackMisItem.setRackId(rackId);
            rackMisItem.setStatus(status);
            rackMisItem.setExpectValue(expectValue);
            redisUtils.hset(RedisKeys.RACK_MIS_LIST, rackId + ":" + locationId, JSONObject.toJSONString(rackMisItem), 3 * 60);
            // 灯警报
            try {
//                wcsService.lightOperation(LightOperationStatus.EQUIPMENT_MIS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }
}
