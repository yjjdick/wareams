package com.ams.job.task;

import com.alibaba.fastjson.JSONArray;
import com.ams.common.utils.RedisUtils;
import com.ams.handler.baiy.PalMisHandler;
import com.ams.handler.baiy.PalQtyHandler;
import com.ams.handler.baiy.PalSetHandler;
import com.ams.service.*;
import com.ams.service.impl.WcsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("amsConsumeTask")
@Slf4j
public class AmsConsumeTask {

    @Autowired
    EntrancePrivilegeService entrancePrivilegeService;

    @Autowired
    DocAsnDetailsService docAsnDetailsService;

    @Autowired
    DocAsnHeaderService docAsnHeaderService;

    @Autowired
    DocOrderHeaderService docOrderHeaderService;

    @Autowired
    DocOrderDetailsService docOrderDetailsService;

    @Autowired
    InvLotLocIdService invLotLocIdService;

    @Autowired
    WcsService wcsService;

    @Autowired
    PalMisHandler palMisHandler;

    @Autowired
    PalQtyHandler palQtyHandler;

    @Autowired
    PalSetHandler palSetHandler;

    @Autowired
    RedisUtils redisUtils;

	public void exec(){
        String msg = redisUtils.rpop("lpWaitMessage");
        if(!StringUtils.isBlank(msg)) {
//            log.info("【lpWaitMessage】msg pop = {}>>>>>>>>>>>>>>>>>>>>>>>>>>>>", msg);
            List<List> dataArr = JSONArray.parseArray(msg, List.class);
            for (List<Object> d : dataArr) {
                String type = (String)d.get(0);
                if (type.equalsIgnoreCase("PAL.MIS")) {
                    palMisHandler.handle(d);
                } else if (type.equalsIgnoreCase("PAL.SET")) {
                    palSetHandler.handle(d);
                } else if (type.equalsIgnoreCase("PAL.QTY")) {
                    palQtyHandler.handle(d);
                }
            }
        }
	}

}
