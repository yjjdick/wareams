package com.ams.job.task;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ams.common.entity.StorageLocation;
import com.ams.common.entity.StorageMatDiffItem;
import com.ams.common.entity.StorageRack;
import com.ams.common.utils.RedisUtils;
import com.ams.enums.EntranceStatus;
import com.ams.enums.OrderStatusEnum;
import com.ams.handler.baiy.PalMisHandler;
import com.ams.handler.baiy.PalQtyHandler;
import com.ams.handler.baiy.PalSetHandler;
import com.ams.model.*;
import com.ams.service.*;
import com.ams.service.impl.WcsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component("amsDispatchTask")
@Slf4j
public class AmsDispatchTask {

    @Autowired
    EntrancePrivilegeService entrancePrivilegeService;

    @Autowired
    DocAsnDetailsService docAsnDetailsService;

    @Autowired
    DocAsnHeaderService docAsnHeaderService;

    @Autowired
    DocOrderHeaderService docOrderHeaderService;

    @Autowired
    DocOrderDetailsService docOrderDetailsService;

    @Autowired
    InvLotLocIdService invLotLocIdService;

    @Autowired
    WcsService wcsService;

    @Autowired
    PalMisHandler palMisHandler;

    @Autowired
    PalQtyHandler palQtyHandler;

    @Autowired
    PalSetHandler palSetHandler;

    @Autowired
    RedisUtils redisUtils;

    Set<String> msgConfig = new HashSet<String>(){{
            add("PAL.MIS");
            add("PAL.SET");
            add("PAL.QTY");
    }};

	public void exec(){
        String data = wcsService.lpWaitMessage();
        if(!StringUtils.isBlank(data) && !data.equals("[]")) {
            List<List> dataArr = JSONArray.parseArray(data, List.class);
            List<List> filterDataArr = new ArrayList<>();
            for (List<Object> d : dataArr) {
                String type = (String)d.get(0);
                if (msgConfig.contains(type)) {
                    filterDataArr.add(d);
                }
            }
            if (filterDataArr != null && filterDataArr.size() > 0) {
                redisUtils.lpush("lpWaitMessage", JSONObject.toJSONString(filterDataArr));
            }
        }
	}

    public void test() {
//        EntrancePrivilege entrancePrivilegeQuery = new EntrancePrivilege();
//        entrancePrivilegeQuery.setEntrancestatus(EntranceStatus.ENTER.getCode());
//        EntrancePrivilege entrancePrivilege = entrancePrivilegeService.findFirstByModel(entrancePrivilegeQuery);
//        if (entrancePrivilege == null) {
//            return;
//        }
//        String orderno = entrancePrivilege.getOrderno();
//        log.info(String.format("任务轮询到有人进入 >>>>>>>>>>>>>>>>>>>>>>>>>> orderno = %s", orderno));
//
//        //获取当前数据库库存列表
//        List<InvLotLocId> invLotLocIdList = invLotLocIdService.findAll();
//        //获取现在立柜中全部库存列表
//        try {
//            wcsService.getRetriveList(false);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        List<StorageRack> storageRackList = null;
//        List<StorageMatDiffItem> storageMatDiffItemList = new ArrayList<>();
//
//        //比对两个集合库存差异得出变动货物
//        for (InvLotLocId invLotLocId : invLotLocIdList) {
//            for (StorageRack storageRack : storageRackList) {
//                for (StorageLocation storageLocation : storageRack.getStorageLocationList()) {
//                    if (invLotLocId.getSku().equals(storageLocation.getMatId())) {
//                        if (invLotLocId.getQty().compareTo(storageLocation.getQuantity()) != 0) {
//                            StorageMatDiffItem storageMatDiffItem = new StorageMatDiffItem();
//                            storageMatDiffItem.setMatId(storageLocation.getMatId());
//                            storageMatDiffItem.setMatName(storageLocation.getMatName());
//                            storageMatDiffItem.setDbQty(invLotLocId.getQty());
//                            storageMatDiffItem.setLotQty(storageLocation.getQuantity());
//                            storageMatDiffItemList.add(storageMatDiffItem);
//                        }
//                    }
//                }
//            }
//        }
//
//        //比对变动货物是否是当前订单中的货物
//        Integer finishedTotalCount = 0;
//        Integer finishedCurCount = 0;
//        if(orderno.indexOf("asn") > -1) {
//            DocAsnDetails docAsnDetailsQuery = new DocAsnDetails();
//            docAsnDetailsQuery.setAsnno(orderno);
//            List<DocAsnDetails> docAsnDetailsList = docAsnDetailsService.findByModel(docAsnDetailsQuery);
//            finishedTotalCount = docAsnDetailsList.size();
//            finishedCurCount = 0;
//            for (StorageMatDiffItem storageMatDiffItem : storageMatDiffItemList) {
//                Boolean isExists = false;
//                for (DocAsnDetails docAsnDetails : docAsnDetailsList) {
//                    if (docAsnDetails.getSku().equals(storageMatDiffItem.getMatId())) {
//                        isExists = true;
//                        BigDecimal diffStock = storageMatDiffItem.getLotQty().subtract(storageMatDiffItem.getDbQty());
//                        if(diffStock.intValue() < 0) {
//                            log.error(String.format("入库操作出现负的库存对比 orderno = %s lotqty = %s dbqty = %s", orderno, storageMatDiffItem.getLotQty().toString(), storageMatDiffItem.getDbQty().toString()));
//                        } else if(docAsnDetails.getReceivedqty().compareTo(diffStock) != 0){
//                            // 更新每个物料实际操作数
//                            docAsnDetails.setReceivedqty(diffStock);
//                            docAsnDetailsService.update(docAsnDetails);
//                        }
//                        // 判断是否订单期望库存和实际库存一直已完成物料入库操作
//                        if(docAsnDetails.getExpectedqty().compareTo(docAsnDetails.getReceivedqty()) == 0) {
//                            finishedCurCount++;
//                        }
//                        break;
//                    }
//                }
//                // 有不包含的物料
//                if (!isExists) {
//                    log.error(String.format("比对变动货物是否是当前订单中的货物 有存在不在订单中的物料 orderno = %s matid = %s", orderno, storageMatDiffItem.getMatId()));
//                    // 亮三色灯
//                }
//            }
//            //如果是比对库存差异看是否已达到需求数量
//            //达到需求数量更新订单为结单
//            if(finishedCurCount == finishedTotalCount) {
//                DocAsnHeader docAsnHeader = docAsnHeaderService.findById(orderno);
//                docAsnHeader.setAsnstatus(OrderStatusEnum.FINISHED.getMessage());
//                Boolean udtSucc = docAsnHeaderService.update(docAsnHeader);
//                //通知上游WDS系统且复位三色灯
//                if(udtSucc) {
//
//                } else {
//                    log.error(String.format("asn结单失败 orderno = %s", orderno));
//                }
//            }
//        } else {
//            DocOrderDetails docOrderDetailsQuery = new DocOrderDetails();
//            docOrderDetailsQuery.setOrderno(orderno);
//            List<DocOrderDetails> docOrderDetailsList = docOrderDetailsService.findByModel(docOrderDetailsQuery);
//            finishedTotalCount = docOrderDetailsList.size();
//            finishedCurCount = 0;
//            for (StorageMatDiffItem storageMatDiffItem : storageMatDiffItemList) {
//                Boolean isExists = false;
//                for (DocOrderDetails docOrderDetails : docOrderDetailsList) {
//                    if (docOrderDetails.getSku().equals(storageMatDiffItem.getMatId())) {
//                        isExists = true;
//                        BigDecimal diffStock = storageMatDiffItem.getLotQty().subtract(storageMatDiffItem.getDbQty());
//                        if (diffStock.intValue() < 0) {
//                            log.error(String.format("入库操作出现负的库存对比 orderno = %s lotqty = %s dbqty = %s", orderno, storageMatDiffItem.getLotQty().toString(), storageMatDiffItem.getDbQty().toString()));
//                        } else if (docOrderDetails.getQtyshipped().compareTo(diffStock) != 0) {
//                            // 更新每个物料实际操作数
//                            docOrderDetails.setQtyshipped(diffStock);
//                            docOrderDetailsService.update(docOrderDetails);
//                        }
//                        // 判断是否订单期望库存和实际库存一直已完成物料入库操作
//                        if (docOrderDetails.getQtyshipped().compareTo(docOrderDetails.getQtyordered()) == 0) {
//                            finishedCurCount++;
//                        }
//                        break;
//                    }
//                }
//                // 有不包含的物料
//                if (!isExists) {
//                    log.error(String.format("比对变动货物是否是当前订单中的货物 有存在不在订单中的物料 orderno = %s matid = %s", orderno, storageMatDiffItem.getMatId()));
//                    // 亮三色灯
//                }
//            }
//            //如果是比对库存差异看是否已达到需求数量
//            //达到需求数量更新订单为结单
//            if(finishedCurCount == finishedTotalCount) {
//                DocOrderHeader docOrderHeader = docOrderHeaderService.findById(orderno);
//                docOrderHeader.setSostatus(OrderStatusEnum.FINISHED.getMessage());
//                Boolean udtSucc = docOrderHeaderService.update(docOrderHeader);
//                //通知上游WDS系统且复位三色灯
//                if(udtSucc) {
//
//                } else {
//                    log.error(String.format("order结单失败 orderno = %s", orderno));
//                }
//            }
//        }
    }
}
