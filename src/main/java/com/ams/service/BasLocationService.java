package com.ams.service;

import com.ams.model.BasLocation;

public interface BasLocationService extends BaseService<BasLocation> {
    public String getMatnoByRackIdAndLotId(String rackId, String locationId);
    public String getTrayIdByRackIdAndSku(String rackId, String sku);
    public String getTrayIdBySku(String sku);
}