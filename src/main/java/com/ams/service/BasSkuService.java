package com.ams.service;

import com.ams.model.BasSku;

public interface BasSkuService extends BaseService<BasSku> {
}