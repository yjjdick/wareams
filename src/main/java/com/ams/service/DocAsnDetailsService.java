package com.ams.service;

import com.ams.model.DocAsnDetails;

public interface DocAsnDetailsService extends BaseService<DocAsnDetails> {
    public boolean updateStatusByOrderNo(String orderNo, String status);
    public boolean updateStatusBySkuModel(DocAsnDetails docAsnDetails);
}