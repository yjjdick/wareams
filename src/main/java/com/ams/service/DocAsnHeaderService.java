package com.ams.service;

import com.ams.model.DocAsnHeader;

public interface DocAsnHeaderService extends BaseService<DocAsnHeader> {
    void stockin();
}