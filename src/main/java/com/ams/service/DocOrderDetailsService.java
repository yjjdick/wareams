package com.ams.service;

import com.ams.model.DocOrderDetails;
import com.jfinal.plugin.activerecord.Db;

public interface DocOrderDetailsService extends BaseService<DocOrderDetails> {
     boolean updateStatusByOrderNo(String orderNo, String status);
     boolean updateStatusBySkuModel(DocOrderDetails docOrderDetails);
}