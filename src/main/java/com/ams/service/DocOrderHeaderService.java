package com.ams.service;

import com.ams.model.DocOrderHeader;

public interface DocOrderHeaderService extends BaseService<DocOrderHeader> {
}