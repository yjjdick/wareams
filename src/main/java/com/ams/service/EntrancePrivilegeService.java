package com.ams.service;

import com.ams.common.entity.InsEntity;
import com.ams.model.EntrancePrivilege;

import java.util.List;

public interface EntrancePrivilegeService extends BaseService<EntrancePrivilege> {
    boolean upadteStatusByStastus(String oldStatus, String newStatus);
}