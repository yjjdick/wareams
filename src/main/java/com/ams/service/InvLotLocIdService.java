package com.ams.service;

import com.ams.model.InvLotLocId;

import java.math.BigDecimal;

public interface InvLotLocIdService extends BaseService<InvLotLocId> {
    BigDecimal getStockByLotId(String rackId, String locationId);
}