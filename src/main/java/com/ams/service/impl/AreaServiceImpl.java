package com.ams.service.impl;

import com.ams.service.AreaService;
import com.ams.dao.AreaDao;
import com.ams.model.Area;
import com.ams.service.AreaService;
import org.springframework.stereotype.Service;


@Service
public class AreaServiceImpl extends BaseServiceImpl<AreaDao, Area> implements AreaService {
}
