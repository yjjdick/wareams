package com.ams.service.impl;

import com.ams.dao.BasLocationDao;
import com.ams.model.BasLocation;
import com.ams.service.BasLocationService;
import org.springframework.stereotype.Service;

@Service
public class BasLocationServiceImpl extends BaseServiceImpl<BasLocationDao, BasLocation> implements BasLocationService {

    @Override
    public String getMatnoByRackIdAndLotId(String rackId, String locationId) {
        BasLocation basLocationQuery = new BasLocation();
        basLocationQuery.setRackId(rackId);
        basLocationQuery.setLocationid(locationId);
        BasLocation basLocation = this.findFirstByModel(basLocationQuery);
        return basLocation.getSku();
    }

    @Override
    public String getTrayIdByRackIdAndSku(String rackId, String sku) {
        BasLocation basLocationQuery = new BasLocation();
        basLocationQuery.setSku(sku);
        basLocationQuery.setRackId(rackId);
        BasLocation basLocation = this.findFirstByModel(basLocationQuery);
        return basLocation.getTrayId();
    }

    @Override
    public String getTrayIdBySku(String sku) {
        BasLocation basLocationQuery = new BasLocation();
        basLocationQuery.setSku(sku);
        BasLocation basLocation = this.findFirstByModel(basLocationQuery);
        return basLocation.getTrayId();
    }
}