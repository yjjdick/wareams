package com.ams.service.impl;

import com.ams.dao.BasSkuDao;
import com.ams.model.BasSku;
import com.ams.service.BasSkuService;
import org.springframework.stereotype.Service;

@Service
public class BasSkuServiceImpl extends BaseServiceImpl<BasSkuDao, BasSku> implements BasSkuService {
}