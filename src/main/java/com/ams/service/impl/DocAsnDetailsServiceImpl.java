package com.ams.service.impl;

import com.ams.dao.DocAsnDetailsDao;
import com.ams.model.DocAsnDetails;
import com.ams.service.DocAsnDetailsService;
import com.jfinal.plugin.activerecord.Db;
import org.springframework.stereotype.Service;

@Service
public class DocAsnDetailsServiceImpl extends BaseServiceImpl<DocAsnDetailsDao, DocAsnDetails> implements DocAsnDetailsService {
    @Override
    public boolean updateStatusByOrderNo(String orderNo, String status) {
        return Db.update("update doc_asn_details t set t.linestatus = ? where t.asnno = ?", status, orderNo) > 0;
    }

    @Override
    public boolean updateStatusBySkuModel(DocAsnDetails docAsnDetails) {
        return Db.update("update doc_asn_details t set t.linestatus = ? where t.asnno = ? and t.sku = ?", docAsnDetails.getLinestatus(), docAsnDetails.getAsnno(), docAsnDetails.getSku()) > 0;
    }

}