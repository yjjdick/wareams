package com.ams.service.impl;

import com.ams.dao.DocAsnHeaderDao;
import com.ams.model.DocAsnHeader;
import com.ams.service.DocAsnHeaderService;
import org.springframework.stereotype.Service;

@Service
public class DocAsnHeaderServiceImpl extends BaseServiceImpl<DocAsnHeaderDao, DocAsnHeader> implements DocAsnHeaderService {
    @Override
    public void stockin() {

    }
}