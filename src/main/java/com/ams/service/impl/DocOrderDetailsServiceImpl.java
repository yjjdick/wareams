package com.ams.service.impl;

import com.ams.dao.DocOrderDetailsDao;
import com.ams.model.DocOrderDetails;
import com.ams.service.DocOrderDetailsService;
import com.jfinal.plugin.activerecord.Db;
import org.springframework.stereotype.Service;

@Service
public class DocOrderDetailsServiceImpl extends BaseServiceImpl<DocOrderDetailsDao, DocOrderDetails> implements DocOrderDetailsService {
    @Override
    public boolean updateStatusByOrderNo(String orderNo, String status) {
        return Db.update("update doc_order_details t set t.linestatus = ? where t.orderno = ?", status, orderNo) > 0;
    }

    @Override
    public boolean updateStatusBySkuModel(DocOrderDetails docOrderDetails) {
        return Db.update("update doc_order_details t set t.linestatus = ? where t.orderno = ? and t.sku = ?", docOrderDetails.getLinestatus(), docOrderDetails.getOrderno(), docOrderDetails.getSku()) > 0;
    }
}