package com.ams.service.impl;

import com.ams.dao.DocOrderHeaderDao;
import com.ams.model.DocOrderHeader;
import com.ams.service.DocOrderHeaderService;
import org.springframework.stereotype.Service;

@Service
public class DocOrderHeaderServiceImpl extends BaseServiceImpl<DocOrderHeaderDao, DocOrderHeader> implements DocOrderHeaderService {
}