package com.ams.service.impl;

import com.ams.common.entity.InsEntity;
import com.ams.dao.EntrancePrivilegeDao;
import com.ams.model.EntrancePrivilege;
import com.ams.service.EntrancePrivilegeService;
import com.jfinal.plugin.activerecord.Db;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntrancePrivilegeServiceImpl extends BaseServiceImpl<EntrancePrivilegeDao, EntrancePrivilege> implements EntrancePrivilegeService {
    @Override
    public boolean upadteStatusByStastus(String oldStatus, String newStatus) {
        Db.update("UPDATE entrance_privilege t SET t.entrancestatus = ? where t.entrancestatus = ?", newStatus, oldStatus);
        return false;
    }
}