package com.ams.service.impl;

import com.ams.dao.InvLotLocIdDao;
import com.ams.model.InvLotLocId;
import com.ams.service.InvLotLocIdService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class InvLotLocIdServiceImpl extends BaseServiceImpl<InvLotLocIdDao, InvLotLocId> implements InvLotLocIdService {
    @Override
    public BigDecimal getStockByLotId(String rackId, String locationId) {
        InvLotLocId query = new InvLotLocId();
        query.setLocationid(locationId);
        InvLotLocId invLotLocId = this.findFirstByModel(query);
        if(invLotLocId != null) {
            return invLotLocId.getQty();
        } else {
            return BigDecimal.ZERO;
        }

    }
}