package com.ams.service.impl;

import com.ams.common.utils.SnConstant;
import com.ams.service.SnService;
import com.ams.dao.SnDao;
import com.ams.enums.SnEnum;
import com.ams.model.Sn;
import com.ams.service.SnService;
import org.springframework.stereotype.Service;


@Service
public class SnServiceImpl extends BaseServiceImpl<SnDao, Sn> implements SnService {

    @Override
    public String generate(SnEnum type) {
        return this.dao.generate(type);
    }

    @Override
    public String generateByPrefix(SnEnum type,String prefix) {
        String code =  this.dao.generate(type);
        return String.format("%s%s", prefix, code);
    }

    @Override
    public String generateDocAsn() {
        return generateByPrefix(SnEnum.DOC_ASN, SnConstant.DOC_ASN_PREFIX);
    }

    @Override
    public String generateDocOrder() {
        return generateByPrefix(SnEnum.DOC_ORDER, SnConstant.DOC_ORDER_PREFIX);
    }

    @Override
    public String generateDocAsnDetail() {
        return generateByPrefix(SnEnum.DOC_ASN_DETAIL, SnConstant.DOC_ASN_DETAIL_PREFIX);
    }

    @Override
    public String generateDocOrderDetail() {
        return generateByPrefix(SnEnum.DOC_ORDER_DETAIL, SnConstant.DOC_ORDER_DETAIL_PREFIX);
    }

}
