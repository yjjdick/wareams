package com.ams.webservice;

import com.ams.common.entity.InsEntity;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: andy.qu
 * Date: 2019/5/13
 */
@WebService(
        serviceName = "Wcs2wdsService",
        targetNamespace = "http://CxfService.webservice.service.wms.modules.tbl.com",
        endpointInterface = "com.ams.webservice.Wcs2wdsServiceImpl"
)
public interface IWcs2wdsService {
    @WebMethod
    String SendWms1TaskJson(String para);

    @WebMethod
    String CancelWms1TaskJson(String data);
}
