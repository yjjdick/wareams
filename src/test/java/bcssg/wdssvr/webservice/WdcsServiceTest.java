package bcssg.wdssvr.webservice;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class WdcsServiceTest {

    @Test
    public void finishWms1Task() {
        WdcsServiceImplService wdcsServiceImplService = new WdcsServiceImplService();
        WdcsService wdcsService = wdcsServiceImplService.getWdcsServiceImplPort();
        String testJson = "{\"data\":[{\n" +
                "\"whno\": \"JAL2\",\n" +
                "\"taskSource\": \"1\",\n" +
                "\"taskno\": \"JAL2A021907200010001\",\n" +
                "\"binno\": \"\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"success\":\"0\",\n" +
                "\"message\":\"\",\n" +
                "\"reserve\": \"{[{\n" +
                "\"matno\": \"MAT00001\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"matText\": \"\"\n" +
                "},{\n" +
                "\"matno\": \"MAT00001\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"matText\": \"\"\n" +
                "}]}\",\n" +
                "\"taskTime\": \"2019-07-20 16:00:00\"\n" +
                "},{\n" +
                "\"whno\": \"JAL2\",\n" +
                "\"taskSource\": \"1\",\n" +
                "\"taskno\": \"JAL2A021907200010002\",\n" +
                "\"binno\": \"\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"success\":\"0\",\n" +
                "\"message\":\"\",\n" +
                "\"reserve\": \"{[{\n" +
                "\"matno\": \"MAT00001\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"matText\": \"\"\n" +
                "},{\n" +
                "\"matno\": \"MAT00001\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"matText\": \"\"\n" +
                "}]}\",\n" +
                "\"taskTime\": \"2019-07-20 16:00:00\"\n" +
                "}]\n" +
                "}\n";
        String rsp = wdcsService.finishWms1TaskJson(testJson);
        log.info("rsp >>>>>>>>> {}", rsp);

    }
}