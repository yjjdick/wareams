package com.ams.common.utils;

import com.alibaba.fastjson.JSONArray;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class RedisUtilsTest {

    @Autowired
    RedisUtils redisUtils;

    @Test
    public void lpush() {
        String data = "[[\"LOG\",1564323526526,1,\"audit.bend\",\"admin logged on from 10.10.10.190:53923 user agent: Apache-HttpClient/4.5.5 (Java/1.8.0_161)\\n@{nid = 1}\"]]";
        List<List> dataArr = JSONArray.parseArray(data, List.class);
//        redisUtils.lpush("lpush", 1);
//        redisUtils.lpush("lpush", 2);
//        redisUtils.lpush("lpush", 3);
//        redisUtils.lpush("lpush", 4);
//        redisUtils.lpush("lpush", 5);
//        String data = redisUtils.rpop("lpush");
//        data = redisUtils.rpop("lpush");
//        data = redisUtils.rpop("lpush");
//        data = redisUtils.rpop("lpush");
//        data = redisUtils.rpop("lpush");
    }

    @Test
    public void get() {
        redisUtils.hset("test","test01","001");
        redisUtils.hset("test","test02","002");
        Long l = redisUtils.hsize("test");
//        String test = redisUtils.hget("test","test01");
    }
}