package com.ams.dao;

import bcssg.wdssvr.webservice.WdcsService;
import bcssg.wdssvr.webservice.WdcsServiceImplService;
import com.alibaba.fastjson.JSONObject;
import com.ams.common.entity.DataInsEntity;
import com.ams.enums.SnEnum;
import com.ams.service.SnService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class SnDaoTest {

    @Autowired
    SnService snService;

    @Autowired
    SnService snService1;

    @Test
    public void generate() {
//        for(int i = 0; i < 20; i++) {
//            String sn = snService.generate(SnEnum.DOC_ASN);
//            String sn1 = snService1.generate(SnEnum.DOC_ASN);
//        }
        WdcsServiceImplService wdcsServiceImplService = new WdcsServiceImplService();
        WdcsService wdcsService = wdcsServiceImplService.getWdcsServiceImplPort();

//        String testJson = "{\"data\": [{\n" +
//                "\t\t\"whno\": \"JAL2\",\n" +
//                "\t\t\"deviceno\": \"JAL2050001\",\n" +
//                "\t\t\"deviceType\": \"05\",\n" +
//                "\t\t\"deviceStatus\":\"0\",\n" +
//                "\t\t\"abnInfo\":\"\",\n" +
//                "\t\t\"updateTime\":\"2019-07-20 16:00:00\",\n" +
//                "\t\t},{\n" +
//                "\t\t\"whno\": \"JAL2\",\n" +
//                "\t\t\"deviceno\": \"JAL2050002\",\n" +
//                "\t\t\"deviceType\": \"05\",\n" +
//                "\t\t\"deviceStatus\":\"0\",\n" +
//                "\t\t\" abnInfo \":\"\",\n" +
//                "\t\t\" updateTime \":\"2019-07-20 16:00:00\",\n" +
//                "\t\t}]\n" +
//                "}\n";
//        String rsp = wdcsService.sendDeviceStatusJson(testJson);
//        log.info("rsp >>>>>>>>> {}", rsp);
        String testJson = "{\"data\":[{\n" +
                "\"whno\": \"JAL2\",\n" +
                "\"taskSource\": \"1\",\n" +
                "\"taskno\": \"JAL2A021907200010001\",\n" +
                "\"binno\": \"\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"success\":\"0\",\n" +
                "\"message\":\"\",\n" +
                "\"reserve\": \"{[{\n" +
                "\"matno\": \"MAT00001\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"matText\": \"\"\n" +
                "},{\n" +
                "\"matno\": \"MAT00001\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"matText\": \"\"\n" +
                "}]}\",\n" +
                "\"taskTime\": \"2019-07-20 16:00:00\"\n" +
                "},{\n" +
                "\"whno\": \"JAL2\",\n" +
                "\"taskSource\": \"1\",\n" +
                "\"taskno\": \"JAL2A021907200010002\",\n" +
                "\"binno\": \"\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"success\":\"0\",\n" +
                "\"message\":\"\",\n" +
                "\"reserve\": \"{[{\n" +
                "\"matno\": \"MAT00001\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"matText\": \"\"\n" +
                "},{\n" +
                "\"matno\": \"MAT00001\",\n" +
                "\"quantity\": \"10\",\n" +
                "\"matText\": \"\"\n" +
                "}]}\",\n" +
                "\"taskTime\": \"2019-07-20 16:00:00\"\n" +
                "}]\n" +
                "}\n";


        String rsp = wdcsService.finishWms1TaskJson(testJson);
        DataInsEntity dataInsEntity = JSONObject.parseObject(rsp, DataInsEntity.class);
        log.info("dataInsEntity >>>>>>>>> {}", dataInsEntity);
    }
}