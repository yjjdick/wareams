package com.ams.service.impl;

import com.ams.common.utils.RedisUtils;
import com.ams.enums.LightOperationStatus;
import com.ams.enums.LinkOperationStatus;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class WcsServiceTest {

    @Autowired
    WcsService wcsService;

    @Autowired
    RedisUtils redisUtils;

    @Test
    public void login() {
        String cookie = wcsService.login();
    }

    @Test
    public void getRetriveList() {
        try {
            wcsService.getRetriveList(false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void openDoor() {
        Boolean openDoor = wcsService.openDoor();
    }

    @Test
    public void getRetriveList1() {
        redisUtils.set("test", "123");
    }

    @Test
    public void setPreAuthDelta() {
        wcsService.setPreAuthDelta("01","A/01", BigDecimal.valueOf(2), BigDecimal.valueOf(2));
    }

    @Test
    public void lpWaitMessage() {
        wcsService.lpWaitMessage();
    }

    @Test
    public void initStock() {
        wcsService.initStock();
    }

    @Test
    public void linkOperation() {
        wcsService.linkOperation("1001", LinkOperationStatus.CALL.getCode());
    }

    @Test
    public void lightOperation() {
        try {
            Boolean b = wcsService.lightOperation(LightOperationStatus.NORMAL);
            log.info("b >>>>>>>>>> {}", b);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void test2() {
        try {
            wcsService.asyncFinishWms1TaskJson("");
            log.info("b >>>>>>>>>>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}