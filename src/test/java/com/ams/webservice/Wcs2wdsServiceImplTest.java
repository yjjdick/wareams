package com.ams.webservice;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ams.common.entity.DataInsEntity;
import com.ams.common.entity.InsDetailEntity;
import com.ams.common.entity.InsEntity;
import com.ams.model.DocAsnDetails;
import com.ams.model.DocAsnHeader;
import com.ams.model.InvLotLocId;
import com.ams.service.DocAsnDetailsService;
import com.ams.service.DocAsnHeaderService;
import com.ams.service.DocOrderDetailsService;
import com.ams.service.DocOrderHeaderService;
import com.ams.service.impl.WcsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class Wcs2wdsServiceImplTest {

    @Autowired
    DocAsnHeaderService docAsnHeaderService;

    @Autowired
    DocOrderHeaderService docOrderHeaderService;

    @Autowired
    DocAsnDetailsService docAsnDetailsService;

    @Autowired
    DocOrderDetailsService docOrderDetailsService;

    @Autowired
    WcsService wcsService;


    @Test
    public void invoice() {
        //在一个方法中连续调用多次WebService接口,每次调用前需要重置上下文
        ClassLoader cl = Thread.currentThread().getContextClassLoader();

        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        System.out.println("信息如下：================");
        printResult(dcf);
        //在调用第二个webservice前，需要重置上下文
//        Thread.currentThread().setContextClassLoader(cl);
//
//        System.out.println("用户的信息如下：================");
//        printUserList(dcf);
    }

    @Test
    public void test0() {
        InvLotLocId invLotLocId = new InvLotLocId();
        invLotLocId.setCustomerid("WRC");
        invLotLocId.setLotnum("b001");
        invLotLocId.setSku("mat_01");
        invLotLocId.setLocationid("l001");
        invLotLocId.setQty(BigDecimal.valueOf(100));
        invLotLocId.save();
    }

    @Test
    public void test1() {
        Date da = new Date();
        List<InsEntity> insEntityList = new ArrayList<>();
        InsEntity insEntity = new InsEntity();
        insEntity.setWhno("JAL2");
        insEntity.setBinnoTo("end");
        insEntity.setBinnoFrom("start");
        insEntity.setRfidCode("aaa");
        insEntity.setBatch("b001");
        insEntity.setStoArea("AO2");
        insEntity.setOperateType("1");
        insEntity.setTaskSource("1");
        insEntity.setPalletLoc("l001");
        insEntity.setTaskLevel(1);
        insEntity.setTaskTime("2019-10-01 10:00:00");
        List<InsDetailEntity> insDetailEntityList = new ArrayList<>();
        for(int i = 1; i < 3; i++) {
            InsDetailEntity insDetailEntity = new InsDetailEntity();
            insDetailEntity.setMatno("123");
            insDetailEntity.setMatText("desc");
            insDetailEntity.setBatch("b001");
            insDetailEntity.setQuantity(10.0F);
            insDetailEntityList.add(insDetailEntity);
        }
        insEntity.setReserve(insDetailEntityList);

        insEntityList.add(insEntity);
        insEntity = new InsEntity();
        insEntity.setWhno("JAL2");
        insEntity.setBinnoTo("2019-10-01 10:00:00");
        insEntity.setBinnoFrom("2019-10-01 10:00:00");
        insEntity.setRfidCode("bbb");
        insEntity.setTaskLevel(0);
        insEntity.setStoArea("AO2");
        insEntity.setOperateType("1");
        insEntity.setTaskSource("2");
        insEntity.setPalletLoc("l001");
        insEntity.setTaskTime("2019-10-01 10:00:00");
        insDetailEntityList = new ArrayList<>();
        for(int i = 1; i < 3; i++) {
            InsDetailEntity insDetailEntity = new InsDetailEntity();
            insDetailEntity.setMatno("124");
            insDetailEntity.setMatText("desc");
            insDetailEntity.setQuantity(10.0F);
            insDetailEntityList.add(insDetailEntity);
        }
        insEntity.setReserve(insDetailEntityList);
        insEntityList.add(insEntity);
        log.info(JSONObject.toJSONString(insEntityList));
    }

    public static void main(String[] args) {
        //在一个方法中连续调用多次WebService接口,每次调用前需要重置上下文
        ClassLoader cl = Thread.currentThread().getContextClassLoader();

        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        System.out.println("信息如下：================");
        printResult(dcf);
//        cancelOrder(dcf);
        //在调用第二个webservice前，需要重置上下文
//        Thread.currentThread().setContextClassLoader(cl);
//
//        System.out.println("用户的信息如下：================");
//        printUserList(dcf);
    }

    private static void printResult(JaxWsDynamicClientFactory dcf){
        Client client = dcf.createClient("http://192.168.2.92:8080/ams/services/Wcs2wdsService?wsdl");
        // 需要密码的情况需要加上用户名和密码
        // client.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
        Object[] objects = new Object[0];
        Date da = new Date();
        try {
//            List<InsEntity> insEntityList = new ArrayList<>();
//            InsEntity insEntity = new InsEntity();
//            insEntity.setWhno("JAL2");
//            insEntity.setBinnoTo("end");
//            insEntity.setBinnoFrom("start");
//            insEntity.setTaskno("t001");
//            insEntity.setRfidCode("aaa");
//            insEntity.setStoArea("l001");
//            insEntity.setOperateType("2");
//            insEntity.setTaskSource("1");
//            insEntity.setPalletLoc("aaa");
//            insEntity.setTaskLevel(1);
//            insEntity.setTaskTime("2019-10-01 10:00:00");
//            List<InsDetailEntity> insDetailEntityList = new ArrayList<>();
//            for (int i = 0; i < 1; i++) {
//                InsDetailEntity insDetailEntity = new InsDetailEntity();
//                insDetailEntity.setMatno("mat_01");
//                insDetailEntity.setMatText("desc");
//                insDetailEntity.setQuantity(10.0F);
//                insDetailEntity.setBatch("b001");
//                insDetailEntityList.add(insDetailEntity);
//            }
//
//            insEntity.setReserve(insDetailEntityList);
//
//            insEntityList.add(insEntity);
//            insEntity = new InsEntity();
//            insEntity.setWhno("JAL2");
//            insEntity.setBinnoTo("2019-10-01 10:00:00");
//            insEntity.setBinnoFrom("2019-10-01 10:00:00");
//            insEntity.setRfidCode("bbb");
//            insEntity.setTaskLevel(0);
//            insEntity.setTaskno("t002");
//            insEntity.setStoArea("l001");
//            insEntity.setOperateType("1");
//            insEntity.setTaskSource("2");
//            insEntity.setPalletLoc("aaa");
//            insEntity.setTaskTime("2019-10-01 10:00:00");
//            insDetailEntityList = new ArrayList<>();
//            for(int i = 1; i < 3; i++) {
//                InsDetailEntity insDetailEntity = new InsDetailEntity();
//                insDetailEntity.setMatno("mat_0" + i);
//                insDetailEntity.setMatText("desc");
//                insDetailEntity.setQuantity(10.0F);
//                insDetailEntity.setBatch("b001");
//                insDetailEntityList.add(insDetailEntity);
//            }
//            insEntity.setReserve(insDetailEntityList);
//            insEntityList.add(insEntity);
//            // invoke("方法名",参数1,参数2,参数3....);

//            String ddd = JSONObject.toJSONString(insEntityList);
            String dd = "{\"data\":[{\"whno\": \"JAL2\",\n" +
                    "  \"taskSource\": \"1\",\n" +
                    "  \"taskno\": \"JAL2A021907200010001\",\n" +
                    "  \"rfidCode\": \"\",\n" +
                    "  \"stoArea\": \"A02\",\n" +
                    "  \"palletLoc\": \"\",\n" +
                    "  \"operateType\": \"0\",\n" +
                    "  \"binnoFrom\": \"\",\n" +
                    "  \"binnoTo\": \"\",\n" +
                    "  \"matno\": \"SKU001\",\n" +
                    "  \"quantity\": \"10\",\n" +
                    "\"batch\":\"\",\n" +
                    "  \"unit\":\"\",\n" +
                    "  \"matText\": \"M10\",\n" +
                    "\"reserve\": [{\n" +
                    "\"matno\": \"SKU001\",\n" +
                    "\"quantity\": \"10\",\n" +
                    "\"matText\": \"\"\n" +
                    "}],\n" +
                    "  \"taskLevel\": \"1\",\n" +
                    "  \"taskTime\": \"2019-07-20 16:00:00\"}]}";

//            List<InsEntity> insEntities = JSONArray.parseArray(dd, InsEntity.class);
            objects = client.invoke("SendWms1TaskJson", dd);
            if(objects.length>0){
                ArrayList<Object> objectList = (ArrayList)objects[0];
                for (Object o:objectList){
                    String jsonString = JSONObject.toJSONString(o);
                    log.info("result >>>>>>>>>>>> {}", jsonString);
                }
            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }


    private static void cancelOrder(JaxWsDynamicClientFactory dcf){
        Client client = dcf.createClient("http://localhost:8080/ams/services/Wcs2wdsService?wsdl");
        // 需要密码的情况需要加上用户名和密码
        // client.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
        Object[] objects = new Object[0];
        Date da = new Date();
        try {
            List<InsEntity> insEntityList = new ArrayList<>();
            InsEntity insEntity = new InsEntity();
            insEntity.setWhno("JAL2");
            insEntity.setBinnoTo("end");
            insEntity.setBinnoFrom("start");
            insEntity.setRfidCode("aaa");
            insEntity.setStoArea("l001");
            insEntity.setOperateType("2");
            insEntity.setTaskSource("1");
            insEntity.setTaskno("t001");
            insEntity.setPalletLoc("aaa");
            insEntity.setTaskLevel(1);
            insEntity.setTaskTime("2019-10-01 10:00:00");

            insEntityList.add(insEntity);
            insEntity = new InsEntity();
            insEntity.setWhno("JAL2");
            insEntity.setBinnoTo("2019-10-01 10:00:00");
            insEntity.setBinnoFrom("2019-10-01 10:00:00");
            insEntity.setRfidCode("bbb");
            insEntity.setTaskLevel(0);
            insEntity.setTaskno("t002");
            insEntity.setStoArea("l001");
            insEntity.setOperateType("1");
            insEntity.setTaskSource("2");
            insEntity.setPalletLoc("aaa");
            insEntity.setTaskTime("2019-10-01 10:00:00");
            insEntityList.add(insEntity);
            // invoke("方法名",参数1,参数2,参数3....);

            objects = client.invoke("CancelWms1TaskJson", JSONObject.toJSONString(insEntityList));
            if(objects.length>0){
                ArrayList<Object> objectList = (ArrayList)objects[0];
                for (Object o:objectList){
                    String jsonString = JSONObject.toJSONString(o);
                    log.info("result >>>>>>>>>>>> {}", jsonString);
                }
            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sendWms1TaskJson() {
        DocAsnHeader docAsnHeader = docAsnHeaderService.findById("asn20190803111716");

        // 通知WDS
        DataInsEntity dataInsEntity = new DataInsEntity();
        InsEntity insEntity = new InsEntity();
        insEntity.setSuccess("0");
        insEntity.setTaskno(docAsnHeader.getAsnreference1());
        insEntity.setTaskLevel(Integer.parseInt(docAsnHeader.getPriority()));
        insEntity.setTaskTime(docAsnHeader.getPriority());
        insEntity.setMessage("");
        insEntity.setTaskSource(docAsnHeader.getHEdi04());
        List<InsEntity> insEntityList = new ArrayList<>();
        DocAsnDetails docAsnDetails = new DocAsnDetails();
        docAsnDetails.setAsnno(docAsnHeader.getAsnno());
        List<DocAsnDetails> docAsnDetailsList = docAsnDetailsService.findByModel(docAsnDetails);
        List<InsDetailEntity> insDetailEntityList = new ArrayList<>();
        for (DocAsnDetails asnDetails : docAsnDetailsList) {
            InsDetailEntity insDetailEntity = new InsDetailEntity();
            insDetailEntity.setBatch(asnDetails.getLotatt04());
            insDetailEntity.setMatno(asnDetails.getSku());
            insDetailEntity.setQuantity(asnDetails.getExpectedqty().floatValue());
            insDetailEntity.setUnit(asnDetails.getUom());
            insDetailEntity.setMatText(asnDetails.getAlternativesku());
            insDetailEntityList.add(insDetailEntity);
        }
        insEntity.setReserve(insDetailEntityList);
        insEntityList.add(insEntity);
        dataInsEntity.setData(insEntityList);
        String finishJson = JSONObject.toJSONString(dataInsEntity);
        Boolean b = wcsService.finishWms1TaskJson(finishJson);
    }
}